unit RachotaClasses;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Laz2_DOM, laz2_XMLRead, fgl, DateUtils, BBUtils;

type

  { TFileList }

  TFileList = class(TStringList)
    constructor Create(Dir: String; FileMask: String = '*'); overload;
  private
    FDirectory: String;
    FFileMask: String;
    procedure SetDirectory(AValue: String);
    procedure SetFileMask(AValue: String);
  public
    procedure Update;
  published
    property Directory: String read FDirectory write SetDirectory;
    property FileMask: String read FFileMask write SetFileMask;
  end;

  { TRachotaFileList }

  TRachotaFileList = class(TFileList)
    constructor Create;
  private
  public
    function GetXmlDocument(Index: Integer): TXMLDocument;
  end;

  TRachotaTaskPriority = (
    rtpNone,
    rtpLow,
    rtpNormal,
    rtpHigh
  );

  TRachotaTaskState = (
    rtsNone,
    rtsUnfinished,
    rtsDone
  );

  { TRachotaTask }

  TRachotaTask = class(TObject)
    FElement: TDOMElement;
    FID: Integer;
    FIsPrivate: Boolean;
    FDuration: TDateTime;
    FPriority: TRachotaTaskPriority;
    FIdle: Boolean;
    FDescription: String;
    FKeyword: String;
    FState: TRachotaTaskState;
  private
    FNotes: String;
    procedure ReadState;
    procedure ReadDuration;
    procedure ReadPriority;
    procedure ReadDescription;
    procedure ReadKeyword;
    procedure ReadNotes;
    procedure ReadIsPrivate;
    procedure ReadIdle;
    procedure SetIsPrivate(AValue: Boolean);
    procedure SetDuration(AValue: TTime);
    procedure SetNotes(AValue: String);
    procedure SetPriority(AValue: TRachotaTaskPriority);
    procedure SetIdle(AValue: Boolean);
    procedure SetDescription(AValue: String);
    procedure SetKeyword(AValue: String);
    procedure SetState(AValue: TRachotaTaskState);
  public
    constructor Create(Element: TDOMElement);
  published
    property Duration: TTime read FDuration write SetDuration;
    property ID: Integer read FID;
    property IsPrivate: Boolean read FIsPrivate write SetIsPrivate;
    property Priority: TRachotaTaskPriority read FPriority write SetPriority;
    property Idle: Boolean read FIdle write SetIdle;
    property Description: String read FDescription write SetDescription;
    property Keyword: String read FKeyword write SetKeyword;
    property State: TRachotaTaskState read FState write SetState;
    property Notes: String read FNotes write SetNotes;
  end;

  TRachotaTaskList = specialize TFPGObjectList<TRachotaTask>;

  { TRachotaDay }

  TRachotaDay = class(TRachotaTaskList)
  private
    FElement: TDOMElement;
    FDayDate: TDate;
    FFinish: TTime;
    FID: Integer;
    FStart: TTime;
    procedure ReadDate;
    procedure ReadID;
    procedure ReadStart;
    procedure ReadFinish;
    procedure ReadTasks;
    procedure SetDayDate(AValue: TDate);
    procedure SetFinish(AValue: TTime);
    procedure SetStart(AValue: TTime);
  public
    constructor Create(Element: TDOMElement); overload;
  published
    property DayDate: TDate read FDayDate write SetDayDate;
    property ID: Integer read FID;
    property Start: TTime read FStart write SetStart;
    property Finish: TTime read FFinish write SetFinish;
  end;

  { TRachotaDayList }

  TRachotaDayList = specialize TFPGObjectList<TRachotaDay>;

  { TRachotaWeek }

  TRachotaWeek = class(TRachotaDayList)
    constructor Create(Doc: TXMLDocument); overload;
  private
    FID: Integer;
    FOS: String;
    FWeekYear: Integer;
    procedure ReadYear(WeekElement: TDOMElement);
    procedure ReadID(WeekElement: TDOMElement);
    procedure ReadDays(WeekElement: TDOMElement);
    procedure SetOS(AValue: String);
    procedure SetWeekYear(AValue: Integer);
  published
    property WeekYear: Integer read FWeekYear write SetWeekYear;
    property ID: Integer read FID;
    property OS: String read FOS write SetOS;
  end;

const
  enWeek = 'week';
  anYear = 'year';
  anID = 'id';
  anOS = 'os';
  { ----------------- }
  enDay = 'day';
  anDate = 'date';
  anStart = 'start';
  anFinish = 'finish';
  { ----------------- }
  enTask = 'task';
  anState = 'state';
  anDuration = 'duration';
  enPriority = 'priority';
  enDescription = 'description';
  enKeyword = 'keyword';
  enNotes = 'notes';
  enPrivate = 'private';
  enIdle = 'idle';

implementation

{ TRachotaDay }

procedure TRachotaDay.SetDayDate(AValue: TDate);
begin
  if FDayDate=AValue then Exit;
  FDayDate:=AValue;
end;

procedure TRachotaDay.SetFinish(AValue: TTime);
begin
  if FFinish=AValue then Exit;
  FFinish:=AValue;
end;

procedure TRachotaDay.SetStart(AValue: TTime);
begin
  if FStart=AValue then Exit;
  FStart:=AValue;
end;

procedure TRachotaDay.ReadDate;
var
  sDate: String;
  FS: TFormatSettings;
begin
  sDate := FElement.AttribStrings[anDate];
  if sDate > '' then
  begin
    try
      FDayDate:=StrToDate(sDate, 'mm-dd-yyyy', '-');
      FS := DefaultFormatSettings;
      FS.ShortDateFormat:='yyyy-mm-dd';
      DebugMsgFmt('Day date: %s', [DateToStr(FDayDate, FS)]);
    except
      on EConvertError do
        WarningFmt('An invalid date (“%s”) has been found.', [sDate]);
    end;
  end
  else
    FDayDate:=0;
end;

procedure TRachotaDay.ReadID;
var
  sID: String;
begin
  sID := FElement.AttribStrings[anID];
  if sID > '' then
  begin
    try
      FID:=StrToInt(sID);
      DebugMsgFmt('Day ID: %d', [FID]);
    except
      on EConvertError do
        WarningFmt('An invalid ID (“%s”) has been found.', [sID]);
    end;
  end
  else
    FDayDate:=0;
end;

procedure TRachotaDay.ReadStart;
var
  sStart: String;
  FS: TFormatSettings;
begin
  sStart := FElement.AttribStrings[anStart];
  if sStart > '' then
  begin
    try
      FS := DefaultFormatSettings;
      FS.ShortTimeFormat:='hh:nn:ss';
      FS.TimeSeparator:=':';
      FStart:=StrToTime(sStart, FS);
      DebugMsgFmt('Day start: %s', [TimeToStr(FStart, FS)]);
    except
      on EConvertError do
        WarningFmt('An invalid start time (“%s”) has been found.', [sStart]);
    end;
  end
  else
    FStart:=0;
end;

procedure TRachotaDay.ReadFinish;
var
  sFinish: String;
  FS: TFormatSettings;
begin
  sFinish := FElement.AttribStrings[anFinish];
  if sFinish > '' then
  begin
    try
      FS := DefaultFormatSettings;
      FS.ShortTimeFormat:='hh:nn:ss';
      FS.TimeSeparator:=':';
      FFinish:=StrToTime(sFinish, FS);
      DebugMsgFmt('Day finish: %s', [TimeToStr(FFinish, FS)]);
    except
      on EConvertError do
        WarningFmt('An invalid finish time (“%s”) has been found.', [sFinish]);
    end;
  end
  else
    FFinish:=0;
end;

procedure TRachotaDay.ReadTasks;
var
  TaskNodes: TDOMNodeList;
  TaskNode: TDOMNode;
  TaskElement: TDOMElement;
  Task: TRachotaTask;
  I: Integer;
begin
  TaskNodes := FElement.GetElementsByTagName(enTask);
  for I := 0 to Pred(TaskNodes.Count) do
  begin
    TaskNode := TaskNodes[I];
    if TaskNode is TDOMElement then
    begin
      TaskElement := TaskNode as TDOMElement;
      Task := TRachotaTask.Create(TaskElement);
      Self.Add(Task);
    end
    else
      Warning('A non-element task node has been found.');
  end;
end;

constructor TRachotaDay.Create(Element: TDOMElement);
begin
  inherited Create();
  FElement := Element;
  // Initialization
  FDayDate:=0;
  FID:=0;
  FFinish:=0;
  FStart:=0;
  // Date
  ReadDate;
  // ID
  ReadID;
  // Start
  ReadStart;
  //Finish
  ReadFinish;
  // Tasks
  ReadTasks;
end;

{ TRachotaTask }

procedure TRachotaTask.ReadState;
var
  sState: String;
  iState: Integer;
begin
  sState:=FElement.AttribStrings[anState];
  if sState > '' then
  begin
    try
      iState:=StrToInt(sState);
      try
        FState:=TRachotaTaskState(iState);
        DebugMsgFmt('Task state: %d', [FState]);
      except
        on EConvertError do
          WarningFmt(
            'Can’t convert the given state (%d) to TRachotaTaskState.',
            [iState]
          );
      end;
    except
      on EConvertError do
        WarningFmt(
          'Invalid task state attribute (“%s”) found.',
          [sState]
        );
    end;
  end;
end;

procedure TRachotaTask.ReadDuration;
var
  sDuration: String;
begin
  sDuration := FElement.AttribStrings[anDuration];
  if sDuration > '' then
  begin
    try
      FDuration:=ScanDateTime('hh:nn:ss', sDuration);
      DebugMsgFmt(
        'Task duration: %s',
        [FormatDateTime('[h]:nn:ss', FDuration, [fdoInterval])]
      );
    except
      on EConvertError do
        WarningFmt(
          'An invalid task duration attribute (“%s”) has been found.',
          [sDuration]
        );
    end;
  end
  else
    FDuration:=0;
end;

procedure TRachotaTask.ReadPriority;
var
  PriorityNodes: TDOMNodeList;
  PriorityNodeCount: Integer;
  PriorityNode: TDOMNode;
  PriorityElement: TDOMElement;
  sPriority: String;
  iPriotity: Integer;
begin
  PriorityNodes := FElement.GetElementsByTagName(enPriority);
  PriorityNodeCount:=PriorityNodes.Count;
  if PriorityNodeCount >= 1 then
  begin
    if PriorityNodeCount > 1 then
    begin
      WarningFmt(
        'More than one task priority elements (%d) found.',
        [PriorityNodeCount]
      );
    end;
    PriorityNode := PriorityNodes[0];
    if PriorityNode is TDOMElement then
    begin
      PriorityElement := PriorityNode as TDOMElement;
      sPriority:=PriorityElement.TextContent;
      if sPriority > '' then
      begin
        try
          iPriotity:=StrToInt(sPriority);
          try
            FPriority:=TRachotaTaskPriority(iPriotity);
            DebugMsgFmt('Task priority: %d', [FPriority]);
          except
            on EConvertError do
              WarningFmt(
                'The given task priority value (%d) canʼt be converted ' +
                'to TRachotaTaskPriority.',
                [iPriotity]
              );
          end;
        except
          on EConvertError do
            WarningFmt(
              'An invalid task priority value (“%s”) has been found',
              [sPriority]
            );
        end;
      end
      else
        Warning('An empty task priority value has been found.');
    end
    else
      Warning('A non-element task priority node has been found.');
  end
  else
    Warning('The given task misses the priority element.');
end;

procedure TRachotaTask.ReadDescription;
var
  DescriptionNodes: TDOMNodeList;
  DescriptionNodeCount: Integer;
  DescriptionNode: TDOMNode;
  DescriptionElement: TDOMElement;
begin
  DescriptionNodes := FElement.GetElementsByTagName(enDescription);
  DescriptionNodeCount:=DescriptionNodes.Count;
  if DescriptionNodeCount > 0 then
  begin
    if DescriptionNodeCount > 1 then
    WarningFmt(
      'More than one (%d) task description nodes have been found. ' +
      'Ignoring the others.',
      [DescriptionNodeCount]
    );
    DescriptionNode := DescriptionNodes[0];
    if DescriptionNode is TDOMElement then
    begin
      DescriptionElement := DescriptionNode as TDOMElement;
      FDescription:=DescriptionElement.TextContent;
      DebugMsgFmt('Task description: “%s”', [FDescription]);
    end
    else
      Warning('A non-element task description node has been found.');
  end
  else // No description node
    Warning('No task description node has been found.');
end;

procedure TRachotaTask.ReadKeyword;
var
  KeywordNodes: TDOMNodeList;
  KeywordNodeCount: Integer;
  KeywordNode: TDOMNode;
  KeywordElement: TDOMElement;
begin
  KeywordNodes := FElement.GetElementsByTagName(enKeyword);
  KeywordNodeCount:=KeywordNodes.Count;
  if KeywordNodeCount > 0 then
  begin
    if KeywordNodeCount > 1 then
    begin
      WarningFmt(
        'More than one task keyword nodes (%d) have been found. ' +
        'Ignoring the others.',
        [KeywordNodeCount]
      );
    end;
    KeywordNode := KeywordNodes[0];
    if KeywordNode is TDOMElement then
    begin
      KeywordElement := KeywordNode as TDOMElement;
      FKeyword:=KeywordElement.TextContent;
      DebugMsgFmt('Task keyword: “%s”', [FKeyword]);
    end
    else
      Warning('A non-element task keyword node has been found.');
  end
  else
    Warning('No task keyword node has been found.');
end;

procedure TRachotaTask.ReadNotes;
var
  NotesNodes: TDOMNodeList;
  NotesNodeCount: Integer;
  NotesNode: TDOMNode;
  NotesElement: TDOMElement;
begin
  NotesNodes := FElement.GetElementsByTagName(enNotes);
  NotesNodeCount:=NotesNodes.Count;
  if NotesNodeCount > 0 then
  begin
    if NotesNodeCount > 1 then
    begin
      WarningFmt(
        'More than one task notes nodes (%d) have been found. ' +
        'Ignoring the others.',
        [NotesNodeCount]
      );
    end;
    NotesNode := NotesNodes[0];
    if NotesNode is TDOMElement then
    begin
      NotesElement := NotesNode as TDOMElement;
      FNotes:=NotesElement.TextContent;
    end
    else
      Warning('A non-element task notes node has been found.');
  end
  {else
    Warning('No task notes node has been found.')};
  DebugMsgFmt('Task notes: “%s”', [FNotes]);
end;

procedure TRachotaTask.ReadIsPrivate;
var
  PrivateNodes: TDOMNodeList;
  PrivateNodeCount: Integer;
  PrivateNode: TDOMNode;
begin
  PrivateNodes := FElement.GetElementsByTagName(enPrivate);
  PrivateNodeCount:=PrivateNodes.Count;
  if PrivateNodeCount > 0 then
  begin
    if PrivateNodeCount > 1 then
    begin
      WarningFmt(
        'More than one task private flag nodes (%d) have been found. ' +
        'Ignoring the others.',
        [PrivateNodeCount]
      );
    end;
    PrivateNode := PrivateNodes[0];
    if PrivateNode is TDOMElement then
    begin
      FIsPrivate:=True;
    end
    else
      Warning('A non-element task private flag node has been found.');
  end;
  DebugMsgFmt('Task private flag: %s', [BoolToStr(FIsPrivate, True)]);
end;

procedure TRachotaTask.ReadIdle;
var
  IdleNodes: TDOMNodeList;
  IdleNodeCount: Integer;
  IdleNode: TDOMNode;
begin
  IdleNodes := FElement.GetElementsByTagName(enIdle);
  IdleNodeCount:=IdleNodes.Count;
  if IdleNodeCount > 0 then
  begin
    if IdleNodeCount > 1 then
    begin
      WarningFmt(
        'More than one task idle flag nodes (%d) have been found. ' +
        'Ignoring the others.',
        [IdleNodeCount]
      );
    end;
    IdleNode := IdleNodes[0];
    if IdleNode is TDOMElement then
    begin
      FIdle:=True;
    end
    else
      Warning('A non-element task idle flag node has been found.');
  end;
  DebugMsgFmt('Task idle flag: %s', [BoolToStr(FIdle, True)]);
end;

procedure TRachotaTask.SetIsPrivate(AValue: Boolean);
begin
  if FIsPrivate <> AValue then
    FIsPrivate:=AValue;
end;

procedure TRachotaTask.SetDuration(AValue: TTime);
begin
  if FDuration <> AValue then
    FDuration:=AValue;
end;

procedure TRachotaTask.SetNotes(AValue: String);
begin
  if FNotes=AValue then Exit;
  FNotes:=AValue;
end;

procedure TRachotaTask.SetPriority(AValue: TRachotaTaskPriority);
begin
  if FPriority <> AValue then
    FPriority:=AValue;
end;

procedure TRachotaTask.SetIdle(AValue: Boolean);
begin
  if FIdle <> AValue then
    FIdle:=AValue;
end;

procedure TRachotaTask.SetDescription(AValue: String);
begin
  if FDescription <> AValue then
    FDescription:=AValue;
end;

procedure TRachotaTask.SetKeyword(AValue: String);
begin
  if FKeyword <> AValue then
    FKeyword:=AValue;
end;

procedure TRachotaTask.SetState(AValue: TRachotaTaskState);
begin
  if FState <> AValue then
    FState:=AValue;
end;

constructor TRachotaTask.Create(Element: TDOMElement);
begin
  inherited Create;

  // Initialization
  FElement := Element;
  FID:=0;
  FIsPrivate:=False;
  FDuration:=0;
  FPriority:=rtpNone;
  FIdle:=False;
  FDescription:='';
  FKeyword:='';
  FState:=rtsNone;

  // State
  ReadState;
  // Duration
  ReadDuration;
  // Priority
  ReadPriority;
  // Description
  ReadDescription;
  // Keyword
  ReadKeyword;
  // Notes
  ReadNotes;
  // IsPrivate
  ReadIsPrivate;
  // Idle
  ReadIdle;
end;

{ TRachotaWeek }

constructor TRachotaWeek.Create(Doc: TXMLDocument);
var
  WeekNodes: TDOMNodeList;
  WeekNode: TDOMNode;
  WeekNodeCount: Integer;
  WeekElement: TDOMElement;
begin
  inherited Create();

  // Initialization
  FWeekYear:=0;
  FID:=0;
  FOS:='';

  WeekNodes := Doc.GetElementsByTagName(enWeek);
  WeekNodeCount:=WeekNodes.Count;
  if WeekNodeCount >= 1 then
  begin
    if WeekNodeCount > 1 then
      WarningFmt(
        'More than one week node (%s) were found. Ignoring the others.',
        [WeekNodeCount]
      );
    WeekNode := WeekNodes[0];
    if WeekNode is TDOMElement then
    begin
      WeekElement := WeekNode as TDOMElement;
      // Year
      ReadYear(WeekElement);
      // ID
      ReadID(WeekElement);
      // OS
      FOS:=WeekElement.GetAttribute(anOS);
      DebugMsgFmt('Week OS: %s', [FOS]);
      // Days
      ReadDays(WeekElement);
    end
    else
      Warning(
        'A non-element week node has been found. ' +
        'Skipping it.'
      );
  end
  else // WeekNodeCount = 0
  begin
    Warning(
      'No week node has been found!'
    );
  end;

end;

procedure TRachotaWeek.ReadYear(WeekElement: TDOMElement);
var
  sYear: String;
begin
  sYear:=WeekElement.GetAttribute(anYear);
  if sYear > '' then
  begin
    try
      FWeekYear:=StrToInt(sYear);
      DebugMsgFmt('Week year: %d', [FWeekYear]);
    except
      on EConvertError do
        WarningFmt('An invalid year (“%s”) has been found.', [sYear]);
    end;
  end
  else // sYear = ''
    Warning('An unspecified or empty year attribute has been found.');
end;

procedure TRachotaWeek.ReadID(WeekElement: TDOMElement);
var
  sID: String;
begin
  sID:=WeekElement.GetAttribute(anID);
  if sID > '' then
  begin
    try
      FID:=StrToInt(sID);
      DebugMsgFmt('Week ID: %d', [FID]);
    except
      on EConvertError do
      begin
        WarningFmt('An invalid ID (“%s”) has been found.', [sID]);
        FID:=0;
      end;
    end;
  end
  else
    Warning('An unspecified or empty ID attribute has been found.');
end;

procedure TRachotaWeek.ReadDays(WeekElement: TDOMElement);
var
  DayNodes: TDOMNodeList;
  DayNode: TDOMNode;
  DayElement: TDOMElement;
  I: Integer;
  RachotaDay: TRachotaDay;
begin
  DayNodes := WeekElement.GetElementsByTagName(enDay);
  for I := 0 to DayNodes.Count - 1 do
  begin
    DayNode := DayNodes[I];
    if DayNode is TDOMElement then
    begin
      DayElement := DayNode as TDOMElement;
      RachotaDay := TRachotaDay.Create(DayElement);
      Self.Add(RachotaDay);
    end
    else
      Warning('A non-element day node has been found.');
  end;
end;

procedure TRachotaWeek.SetWeekYear(AValue: Integer);
begin
  if FWeekYear=AValue then Exit;
  FWeekYear:=AValue;
end;

procedure TRachotaWeek.SetOS(AValue: String);
begin
  if FOS=AValue then Exit;
  FOS:=AValue;
end;

{ TFileList }

constructor TFileList.Create(Dir: String; FileMask: String);
begin
  inherited Create;
  FDirectory:=Dir;
  FFileMask:=FileMask;
  Self.Update;
end;

procedure TFileList.SetDirectory(AValue: String);
begin
  FDirectory:=AValue;
  Self.Update;
end;

procedure TFileList.SetFileMask(AValue: String);
begin
  FFileMask:=AValue;
  Self.Update;
end;

procedure TFileList.Update;
begin
  FindAllFiles(Self, FDirectory, FFileMask, False);
end;

{ TRachotaFileList }

constructor TRachotaFileList.Create;
var
  UserHome: String;
  RachotaDir: String;
begin
  UserHome:=GetUserDir;
  RachotaDir:=UserHome + PathDelim + '.rachota';
  inherited Create(RachotaDir, 'diary_*_*.xml');
end;

function TRachotaFileList.GetXmlDocument(Index: Integer): TXMLDocument;
begin
  ReadXMLFile(Result, Self[Index]);
end;

end.

