unit MainForm;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Contnrs, Forms, Controls, Graphics, Dialogs, Menus,
  StdCtrls, DBCtrls, DBGrids, ExtCtrls, Buttons, ExtDlgs,
  WerxTypes, WerxClasses,
  WerxComponentsDataModule, WerxGrid, WerxNavigator, WerxPanel,
  WerxAutoData, WerxEdit, WerxCombo, WerxMemo, WerxCheckBox, GlowingButton,
  BBUtils, sqldb,
  CategoriesForm;

type

  TBBTaskAction = (
    bbtaNew,
    bbtaEdit,
    bbtaCopy,
    bbtaDelete
  );

  TBBTaskKey = record
    Date: TDate;
    ID: Integer;
  end;

  { TFMain }

  TFMain = class(TForm)
    btImport: TButton;
    btOK: TBitBtn;
    btCategories: TButton;
    btSave: TBitBtn;
    CalDlg: TCalendarDialog;
    cbCategory: TWerxCombo;
    cbPrivate: TWerxCheckBox;
    edDate: TWerxEdit;
    edDuration: TWerxEdit;
    edID: TWerxEdit;
    edName: TWerxEdit;
    gbWorking: TGlowingButton;
    GridMenu: TPopupMenu;
    ilWorking: TImageList;
    lbCategory: TLabel;
    lbDate: TLabel;
    lbDuration: TLabel;
    lbID: TLabel;
    lbName: TLabel;
    lbNotes: TLabel;
    memoLog: TMemo;
    mmNotes: TWerxMemo;
    Navigator: TWerxNavigator;
    Grid: TWerxGrid;
    AutoData: TWERXAutoData;
    panGrid: TPanel;
    QID: TSQLQuery;
    panTop: TPanel;
    Timer: TTimer;
    TrayIcon: TTrayIcon;
    WerxNavigator: TWerxNavigator;
    WerxPanel: TWerxPanel;
    procedure btCategoriesClick(Sender: TObject);
    procedure btImportClick(Sender: TObject);
    procedure btOKClick(Sender: TObject);
    procedure btSaveClick(Sender: TObject);
    procedure edDateButtonClick(Sender: TObject);
    procedure edDateChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure gbWorkingStateChange(Sender: TObject);
    procedure TimerTimer(Sender: TObject);
    procedure TrayIconClick(Sender: TObject);
    procedure WerxNavigatorButtonClick(Sender: TObject;
      Button: TNavigationButtonKind);
    procedure WerxPanelClose(Sender: TObject; var Allow: Boolean);
  private
    FTaskAction: TBBTaskAction;
    OldDate: TDate;
    procedure SetTaskAction(Value: TBBTaskAction);
  public
    procedure NewTask;
    procedure EditTask;
    procedure CopyTask;
    procedure DeleteTask;
    procedure SaveData;
    function GetCurrentTaskKey: TBBTaskKey;
    function GetNewTaskKey: TBBTaskKey;
    function GetFreeID(TaskDate: TDate): Integer;
    function GetEditingControls: TComponentList;
    procedure EnableEditingControls;
    procedure DisableEditingControls;
    procedure SetEditingControlsEnablement(Value: Boolean);
  published
    property TaskAction: TBBTaskAction read FTaskAction write SetTaskAction;
  end;

var
  FMain: TFMain;

const
  TaskActionTitles: array[TBBTaskAction] of String = (
    'New Task',
    'Edit Task',
    'Copy Task',
    'Delete Task'
  );

implementation

uses
  Clipbrd, DataModule, BBClasses, RachotaClasses, Laz2_DOM;

{$R *.lfm}

{ TFMain }

procedure TFMain.btImportClick(Sender: TObject);
var
  Response: TModalResult;
  RachotaFileList: TRachotaFileList;
  W, D, T: Integer;
  Doc: TXMLDocument;
  FilePath: String;
  RachotaWeek: TRachotaWeek;
  RachotaDay: TRachotaDay;
  RachotaTask: TRachotaTask;
  Category: TTaskCategory;
  Task: TTask;
  //GlobalTaskCounter: Integer;
  DayTaskCounter: Integer;
begin
  Response := MessageDlg(
    'Import from Rachota',
    'Are you sure you want to import data from your local Rachota database? ' +
    LineEnding +
    'WARNING: Please do it on an empty BBD database ' +
    'since else your existing records may be randomly overwritten!',
    mtWarning,
    mbYesNo,
    0
  );
  if Response = mrNo then
    Exit;

  if DM.Connect then
  begin
    // Initialization
    //GlobalTaskCounter:=0;

    RachotaFileList := TRachotaFileList.Create;
    for W := 0 to Pred(RachotaFileList.Count) do
    begin
      FilePath:=RachotaFileList[W];
      DebugMsgFmt('——— Reading week file “%s”… ———', [FilePath]);
      Doc:=RachotaFileList.GetXmlDocument(W);
      RachotaWeek := TRachotaWeek.Create(Doc);
      for D := 0 to Pred(RachotaWeek.Count) do
      begin
        RachotaDay := RachotaWeek[D];
        DayTaskCounter:=0;
        for T := 0 to Pred(RachotaDay.Count) do
        begin
          RachotaTask := RachotaDay[T];
          if not RachotaTask.Idle then
          begin
            Category := TTaskCategory.Create; // ID = -1
            Category.Name:=RachotaTask.Keyword;
            DM.CategoryList.AddCategory(Category);
            Task := RachotaTask; { assigned via an operator
              defined in the DataModule unit; creates a new TTask }
            {Task.ID:=GlobalTaskCounter;} { Initialize the ID for repeated
              imports }
            //Task.ID:=T;
            Task.ID:=DayTaskCounter;
            Task.TaskDate:=RachotaDay.DayDate;
            Task.CategoryID:=Category.ID;
            DM.SaveTask(Task);
            FreeAndNil(Task);
            //Inc(GlobalTaskCounter);
            Inc(DayTaskCounter);
            Application.ProcessMessages;
          end;
        end;
      end;
      RachotaWeek.Free;
      Doc.Free;
    end;
    DM.SaveCategoryList;
    Grid.ViewData();
    DebugMsg('Finished.');
  end;
end;

procedure TFMain.btCategoriesClick(Sender: TObject);
var
  OldID, NewID: Integer;
begin
  try
    OldID:=cbCategory.AsInteger;
  except
    OldID:=-1;
  end;
  NewID:=FrmCategories.Execute(OldID);
  cbCategory.AsInteger:=NewID;
end;

procedure TFMain.btOKClick(Sender: TObject);
begin
  SaveData;
  WerxPanel.Hide;
  Grid.ViewData();
end;

procedure TFMain.btSaveClick(Sender: TObject);
begin
  SaveData;
  TaskAction:=bbtaEdit;
  Grid.ViewData();
end;

procedure TFMain.edDateButtonClick(Sender: TObject);
begin
  CalDlg.Date := edDate.AsDate;
  if CalDlg.Execute then
  begin
    edDate.AsDate:=CalDlg.Date;
  end;
end;

procedure TFMain.edDateChange(Sender: TObject);
var
  NewDate: TDate;
begin
  if FTaskAction <> bbtaDelete then
  begin
    NewDate:=edDate.AsDate;
    if NewDate <> OldDate then
    begin
      if Assigned(edID) then
         edID.AsInteger:=GetFreeID(NewDate);
    end
    else
    begin
      if Assigned(edID) then
        edID.AsString:=edID.OldValue;
    end;
  end;
end;

procedure TFMain.FormCreate(Sender: TObject);
begin
  BBUtils.DebugMemo := memoLog;
  TaskAction:=bbtaNew;
end;

procedure TFMain.FormShow(Sender: TObject);
begin
  if DM.Connect then
  begin
    Grid.ViewData();
    Grid.Last;
  end;
end;

procedure TFMain.gbWorkingStateChange(Sender: TObject);
begin
  if gbWorking.State = gbsPressed then
    Timer.Enabled:=True
  else
    Timer.Enabled:=False;
end;

procedure TFMain.TimerTimer(Sender: TObject);
var
  OldTime, NewTime, Second: TTime;
begin
  Second:=EncodeTime(0, 0, 1, 0);
  try
    OldTime:=edDuration.AsTime;
  except
    on E: EConvertError do
      OldTime:=0;
  end;
  // Add the timer interval in seconds
  NewTime:=OldTime + Second * (Timer.Interval div 1000);
  // Write & show the new time
  edDuration.AsTime:=NewTime;
  {
    The I/O responsiveness slowly decreases after several tens of minutes
    on Linux.
    Didn't find the cause yet.
    Tried setting the 'Modified' property to False
    in hope that the growing undo buffer is causing the lags
    but it apparently doesn't.
    Lowered the tick frequency to once per 5 seconds.
    This helps but doesn't solve the issue.
  }
  // edDuration.Modified:=False;
end;

procedure TFMain.TrayIconClick(Sender: TObject);
begin
  if Self.WindowState = wsMinimized then
    Self.WindowState:=wsNormal
  else
    Self.WindowState:=wsMinimized;
end;

procedure TFMain.WerxNavigatorButtonClick(Sender: TObject;
  Button: TNavigationButtonKind);
begin
  case Button of
  nbkAdd: NewTask;
  nbkEdit: EditTask;
  nbkCopy: CopyTask;
  nbkDelete: DeleteTask;
  end;
end;

procedure TFMain.WerxPanelClose(Sender: TObject; var Allow: Boolean);
var
  Response: TModalResult;
begin
  if TaskAction = bbtaDelete then
    Allow:=True // allow to close the panel and don’t delete the data
  else
  begin
    // Ask for permission
    Response:=MessageDlg(
      'Closing editation',
      'Do you want to exit the task editation? ' + LineEnding +
      'The data won’t be saved!',
      mtWarning,
      mbOKCancel, 0,
      mbOK
    );
    if Response = mrOK { close permitted } then
    begin
      Allow:=True; // allow to close the panel and don’t save the data
      Timer.Enabled:=False; // stop the timer
    end
    else // close forbidden
      Allow:=False; // don’t close the panel and continue editing
  end;
end;

procedure TFMain.SetTaskAction(Value: TBBTaskAction);
begin
  FTaskAction:=Value;
  WerxPanel.Title:=TaskActionTitles[FTaskAction];
  if Value = bbtaDelete then
    DisableEditingControls
  else
    EnableEditingControls;
end;

procedure TFMain.NewTask;
var
  Key: TBBTaskKey;
begin
  // Set the task action and the corresponding title
  TaskAction:=bbtaNew;
  // Clear the editing controls
  AutoData.ClearData;
  // Fill the category list
  cbCategory.ReadList(AutoData.Database);
  // Get a new task key
  Key := GetNewTaskKey;
  // Prepare the task date for data load
  edDate.AsDate:=Key.Date;
  // Save the date for future use
  OldDate:=Key.Date;
  // Prepare the task ID for task creation
  edID.AsInteger:=Key.ID;
  // Show the panel
  WerxPanel.CenterInParent;
  WerxPanel.Show;
end;

procedure TFMain.EditTask;
var
  Key: TBBTaskKey;
begin
  // Set the task action and the corresponding title
  TaskAction:=bbtaEdit;
  // Get the current task key
  Key := GetCurrentTaskKey;
  // Prepare the task date for data load
  edDate.AsDate:=Key.Date;
  // Save the date for future use
  OldDate:=Key.Date;
  // Prepare the task ID for data load
  edID.AsInteger:=Key.ID;
  // Load the task data
  AutoData.LoadData;
  // Show the panel
  WerxPanel.CenterInParent;
  WerxPanel.Show;
end;

procedure TFMain.CopyTask;
var
  Key: TBBTaskKey;
  NewID: Integer;
begin
  // Set the task action and the corresponding title
  TaskAction:=bbtaCopy;
  // Get the current task key
  Key := GetCurrentTaskKey;
  // Prepare the task date for data load
  edDate.AsDate:=Key.Date;
  // Save the date for future use
  OldDate:=Key.Date;
  // Prepare the task ID for data load
  edID.AsInteger:=Key.ID;
  // Load the task data
  AutoData.LoadData;
  // Fetch a new task ID
  NewID:=GetFreeID(Key.Date);
  // Set the task ID to the new one
  edID.AsInteger:=NewID;
  // Show the panel
  WerxPanel.CenterInParent;
  WerxPanel.Show;
end;

procedure TFMain.DeleteTask;
var
  Key: TBBTaskKey;
begin
  // Set the task action and the corresponding title
  TaskAction:=bbtaDelete;
  // Get the current task key
  Key := GetCurrentTaskKey;
  // Prepare the task date for data load
  edDate.AsDate:=Key.Date;
  // Save the date for future use
  OldDate:=Key.Date;
  // Prepare the task ID for data load
  edID.AsInteger:=Key.ID;
  // Load the task data
  AutoData.LoadData;
  // Show the panel
  WerxPanel.CenterInParent;
  WerxPanel.Show;
end;

procedure TFMain.SaveData;
begin
  case FTaskAction of
  TBBTaskAction.bbtaNew, TBBTaskAction.bbtaCopy:
    AutoData.SaveDataViaInsert(False);
  TBBTaskAction.bbtaEdit:
    AutoData.SaveDataViaUpdate(False);
  TBBTaskAction.bbtaDelete:
    AutoData.DeleteData(False);
  else
  end;
end;

function TFMain.GetCurrentTaskKey: TBBTaskKey;
begin
  Result.Date:=Grid.Query.FieldByName('taskdate').AsDateTime;
  Result.ID:=Grid.Query.FieldByName('id').AsInteger;
end;

function TFMain.GetNewTaskKey: TBBTaskKey;
begin
  Result.Date:=Date;
  Result.ID:=GetFreeID(Result.Date);
end;

function TFMain.GetFreeID(TaskDate: TDate): Integer;
{ TODO : Maybe base this on a stored procedure? }
var
  ID: Integer;
  Found: Integer;
  IsFree: Boolean;
begin
  ID:=-1;
  // Prepare the query
  QID.SQL.Text:='select count(*) as found from tasks ' +
    'where taskdate = :taskdate and id = :id';
  repeat
    Inc(ID);
    // Fill in the params
    QID.ParamByName('taskdate').AsDate:=TaskDate;
    QID.ParamByName('id').AsInteger:=ID;
    // Get the found records count for the proposed date & id
    // from the first record (should be always present)
    // 0 -> not found -> The ID is free, let's take it.
    // 1 ->     found -> The ID is taken, let's continue our search.
    // more -> What the heck? Impossible!
    QID.Open;
    QID.First;
    Found:=QID.FieldByName('found').AsInteger;
    IsFree:=Found = 0;
    QID.Close;
  until IsFree;
  Result := ID;
end;

function TFMain.GetEditingControls: TComponentList;
var
  C: Integer;
  Ctl: TControl;
begin
  Result := TComponentList.Create(False);
  for C := 0 to Pred(WerxPanel.ControlCount) do
  begin
    Ctl := WerxPanel.Controls[C];
    if (Ctl <> WerxPanel.TitleBar) and
       (Ctl <> WerxPanel.Closer) and
       (Ctl <> btOK) then
    begin
      Result.Add(Ctl);
    end;
  end;
end;

procedure TFMain.EnableEditingControls;
begin
  SetEditingControlsEnablement(True);
end;

procedure TFMain.DisableEditingControls;
begin
  SetEditingControlsEnablement(False);
end;

procedure TFMain.SetEditingControlsEnablement(Value: Boolean);
var
  CtlList: TComponentList;
  C: Integer;
  Comp: TComponent;
begin
  CtlList := GetEditingControls;
  for C := 0 to Pred(CtlList.Count) do
  begin
    Comp := CtlList[C];
    if Comp is TControl then
      (Comp as TControl).Enabled:=Value;
  end;
  FreeAndNil(CtlList);
end;

end.

