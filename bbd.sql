create schema if not exists dba;

create table if not exists categories(
  id integer primary key,
  name varchar
);

create table if not exists tasks(
  id integer not null primary key,
  taskdate date not null,
  name character varying not null,
  category integer not null default -1,
  duration time not null,
  notes text,
  isprivate bool
);

create table if not exists dba.s_gridcolumns(
	gridname character varying(255) not null,
	fieldname character varying(255) not null,
  indexc int not null,
	width int not null default 100,
	title_caption character varying(255),
	color int default x'FFFFFF'::int,
	alignment character varying(14) not null default 'taLeftJustify',
  format character varying(255),
  primary key (gridname, fieldname)
);

-- id
insert into dba.s_gridcolumns(
  gridname, fieldname, indexc, title_caption,
  color, alignment
)
values (
  'Tasks', 'id', 0, 'ID',
  x'FFFFFF'::int, 'taRightJustify'
);
-- taskdate
insert into dba.s_gridcolumns(
  gridname, fieldname, indexc, title_caption,
  color, alignment
)
values (
  'Tasks', 'taskdate', 1, 'Date',
  x'7FFFFF'::int, 'taRightJustify'
);
-- name
insert into dba.s_gridcolumns(
  gridname, fieldname, indexc, title_caption,
  color, alignment
)
values (
  'Tasks', 'name', 2, 'Name',
  x'00FF00'::int, 'taLeftJustify'
);
-- category
insert into dba.s_gridcolumns(
  gridname, fieldname, indexc, title_caption,
  color, alignment
)
values (
  'Tasks', 'category', 3, 'Category',
  x'FFFF00'::int, 'taRightJustify'
);
-- duration
insert into dba.s_gridcolumns(
  gridname, fieldname, indexc, title_caption,
  color, alignment
)
values (
  'Tasks', 'duration', 4, 'Duration',
  x'FF00FF'::int, 'taRightJustify'
);
-- notes
insert into dba.s_gridcolumns(
  gridname, fieldname, indexc, title_caption,
  color, alignment
)
values (
  'Tasks', 'notes', 5, 'Notes',
  x'00FFFF'::int, 'taLeftJustify'
);
-- isprivate
insert into dba.s_gridcolumns(
  gridname, fieldname, indexc, title_caption,
  color, alignment
)
values (
  'Tasks', 'isprivate', 6, 'Private',
  x'7F7F7F'::int, 'taLeftJustify'
);

create table if not exists dba.s_userscolumns (
	gridname character varying(255) not null,
	fieldname character varying(255) not null,
	user_name character varying(255) not null default 'dba',
	width int,
	primary key (gridname, fieldname, user_name)
);
