#!/usr/bin/python3 # pylint: disable=too-many-lines,invalid-name

"""
Imports data from the BusyBeeDiary application database
into a LibreOffice Calc sheet.
"""

import os
import locale
import math
import datetime
#from cigydd import printdir
#from cigydd import dirtext
import uno # pylint: disable = import-error
import unohelper # pylint: disable = import-error
#from com.sun.star.awt import Rectangle # pylint: disable = import-error
#from com.sun.star.awt import WindowDescriptor # pylint: disable = import-error

#from com.sun.star.awt.WindowClass import MODALTOP
#from com.sun.star.awt.VclWindowPeerAttribute import OK, OK_CANCEL, YES_NO, \
#  YES_NO_CANCEL, RETRY_CANCEL, DEF_OK, DEF_CANCEL, DEF_RETRY, DEF_YES, DEF_NO

from com.sun.star.awt import MessageBoxButtons as MSG_BUTTONS # pylint: disable = import-error

from com.sun.star.awt import FontWeight # pylint: disable = import-error

#from com.sun.star.text import VertOrientation # pylint: disable = import-error
#from com.sun.star.text import HoriOrientation # pylint: disable = import-error

from com.sun.star.task import XJobExecutor # pylint: disable = import-error

from com.sun.star.awt.PosSize import POS, SIZE, POSSIZE # pylint: disable = import-error
from com.sun.star.awt.PushButtonType import OK, CANCEL # pylint: disable = import-error
from com.sun.star.util.MeasureUnit import TWIP # pylint: disable = import-error

from com.sun.star.connection import NoConnectException # pylint: disable = import-error

BBD_FIELDNAME_TASKDATE = "taskdate"
BBD_FIELDNAME_ID = "id"
BBD_FIELDNAME_DURATION = "duration"
BBD_FIELDNAME_CATEGORY = "category"
BBD_FIELDNAME_NAME = "name"
BBD_FIELDNAME_NOTES = "notes"
BBD_FIELDNAME_ISPRIVATE = "isprivate"

BBD_FIELDS = [
    BBD_FIELDNAME_TASKDATE,
    #BBD_FIELDNAME_ID,
    BBD_FIELDNAME_DURATION,
    BBD_FIELDNAME_CATEGORY,
    BBD_FIELDNAME_NAME,
    BBD_FIELDNAME_NOTES,
    #BBD_FIELDNAME_ISPRIVATE
]

BBD_COLNAME_TASKDATE = "taskdate"
BBD_COLNAME_ID = "id"
BBD_COLNAME_LUMPDURATION = "lumpduration"
BBD_COLNAME_REWARDDURATION = "rewardduration"
BBD_COLNAME_OFFICEDURATION = "officeduration"
BBD_COLNAME_DURATION = "duration"
BBD_COLNAME_REAL_DURATION_HMS = "real duration (h.m.s)"
BBD_COLNAME_REAL_DURATION_H = "real duration (h)"
BBD_COLNAME_BILLED_DURATION_HMS = "billed duration (h.m.s)"
BBD_COLNAME_BILLED_DURATION_H = "billed duration (h)"
BBD_COLNAME_EDITATION_DURATION = "editation duration"
BBD_COLNAME_ADMINISTRATION_DURATION = "administration duration"
BBD_COLNAME_CATEGORY = "category"
BBD_COLNAME_NAME = "name"
BBD_COLNAME_NOTES = "notes"
BBD_COLNAME_ISPRIVATE = "isprivate"

BBD_COLUMNFIELDS = {
    BBD_COLNAME_TASKDATE:                   BBD_FIELDNAME_TASKDATE,
    BBD_COLNAME_LUMPDURATION:               BBD_FIELDNAME_DURATION,
    BBD_COLNAME_REWARDDURATION:             BBD_FIELDNAME_DURATION,
    BBD_COLNAME_OFFICEDURATION:             BBD_FIELDNAME_DURATION,
    BBD_COLNAME_DURATION:                   BBD_FIELDNAME_DURATION,
    BBD_COLNAME_REAL_DURATION_HMS:          BBD_FIELDNAME_DURATION,
    BBD_COLNAME_REAL_DURATION_H:            BBD_FIELDNAME_DURATION,
    BBD_COLNAME_BILLED_DURATION_HMS:        BBD_FIELDNAME_DURATION,
    BBD_COLNAME_BILLED_DURATION_H:          BBD_FIELDNAME_DURATION,
    BBD_COLNAME_EDITATION_DURATION:         BBD_FIELDNAME_DURATION,
    BBD_COLNAME_ADMINISTRATION_DURATION:    BBD_FIELDNAME_DURATION,
    BBD_COLNAME_CATEGORY:                   BBD_FIELDNAME_CATEGORY,
    BBD_COLNAME_NAME:                       BBD_FIELDNAME_NAME,
    BBD_COLNAME_NOTES:                      BBD_FIELDNAME_NOTES,
}

BBD_HMS_COLNAMES = {
    BBD_COLNAME_REAL_DURATION_H:    BBD_COLNAME_REAL_DURATION_HMS,
    BBD_COLNAME_BILLED_DURATION_H:  BBD_COLNAME_BILLED_DURATION_HMS
}

BBD_HEADINGS = {
    BBD_COLNAME_TASKDATE:                   "Datum",
    BBD_COLNAME_ID:                         "Číslo",
    BBD_COLNAME_NAME:                       "Náplň práce",
    BBD_COLNAME_CATEGORY:                   "Kategorie",
    BBD_COLNAME_LUMPDURATION:               "Paušál",
    BBD_COLNAME_REWARDDURATION:             "Odměna",
    BBD_COLNAME_OFFICEDURATION:             "Odm. kanc.",
    BBD_COLNAME_DURATION:                   "Čas",
    BBD_COLNAME_REAL_DURATION_HMS:          "Skutečný čas (h.m.s)",
    BBD_COLNAME_REAL_DURATION_H:            "Skutečný čas (h)",
    BBD_COLNAME_BILLED_DURATION_HMS:        "Účtovaný čas (h.m.s)",
    BBD_COLNAME_BILLED_DURATION_H:          "Účtovaný čas (h)",
    BBD_COLNAME_EDITATION_DURATION:         "Editace",
    BBD_COLNAME_ADMINISTRATION_DURATION:    "Správa",
    BBD_COLNAME_NOTES:                      "Poznámky",
    BBD_COLNAME_ISPRIVATE:                  "Soukromá úloha"
}

REPORT_PARAM_NAME_AGENDA = "agenda"
REPORT_PARAM_NAME_YEAR   = "year"
REPORT_PARAM_NAME_MONTH  = "month"

AGENDA_NAME_KORANDA_CONGREGATION = "Korandův sbor"
AGENDA_NAME_WEST_BOHEMIAN_SENIORATE = "ZČS ČCE"
AGENDA_NAME_EVANGNET = "Evangnet"
AGENDA_NAME_ICEBERG = "Ledovec"
AGENDA_NAME_SNOW = "Iniciativa Sníh"
AGENDA_NAME_WERX = "WERX"
AGENDA_NAMES = [
    AGENDA_NAME_KORANDA_CONGREGATION,
    AGENDA_NAME_WEST_BOHEMIAN_SENIORATE,
    AGENDA_NAME_EVANGNET,
    AGENDA_NAME_ICEBERG,
    AGENDA_NAME_SNOW,
    AGENDA_NAME_WERX
]

MDASH = chr(0x2014)

CTX = uno.getComponentContext()
SM = CTX.getServiceManager()

def get_enum_values(uno_enum):
    "Converts an Uno enumeration object into a dictionary"
    ctx = uno.getComponentContext()
    tdm = ctx.getByName(
            "/singletons/com.sun.star.reflection.theTypeDescriptionManager")
    v = tdm.getByHierarchicalName(uno_enum)
    return dict(zip(v.getEnumNames(), v.getEnumValues()))

CellHoriJustify = get_enum_values('com.sun.star.table.CellHoriJustify')
CellVertJustify = get_enum_values('com.sun.star.table.CellVertJustify')

def get_year_and_month_tuple(date):
    """
    Takes a date string and returns the year and month parts
    as a tuple of numbers
    """
    if (date is not None) and (date.find('-') > -1):
        dateparts = date.split('-')
    else:
        dateparts = (0, 0)
    s_year = dateparts[0]
    s_month = dateparts[1]
    year = int(s_year)
    month = int(s_month)
    yearmonth = (year, month)
    return yearmonth

def get_local_month_names():
    """
    Builds a list of locale-defined month names
    """
    # Get the current time
    now = datetime.datetime.now()
    # Set the locale to the system default
    locale.setlocale(locale.LC_ALL, '')
    # Generate a localized month list
    result = [
        datetime.datetime(now.year, m, 1).strftime('%OB')
        for m in range(1, 13)
    ]
    return result

class DayDelta(datetime.timedelta):
    """A simple class for work with a time delta as a day fraction"""
    @classmethod
    def parse_hours(cls, string):
        """Parse an hours[:minutes[:seconds]] string, creating a new DayDelta"""
        parts = string.split(':')
        parts_count = len(parts)
        if parts_count >= 1:
            s_hours = parts[0]
            f_hours = float(s_hours)
        if parts_count >= 2:
            s_minutes = parts[1]
            f_minutes = float(s_minutes)
        if parts_count >= 3:
            s_seconds = parts[2]
            f_seconds = float(s_seconds)
        result = cls(hours=f_hours, minutes=f_minutes, seconds=f_seconds)
        return result

    @property
    def total_days(self):
        "Computes the total days as a float"
        seconds_per_day = datetime.timedelta(days=1).total_seconds()
        result = self.total_seconds() / seconds_per_day
        return result

    @property
    def total_whole_hours_in_days(self):
        "Computes the total number of whole hours in the interval represented"
        seconds_per_hour = datetime.timedelta(hours=1).total_seconds()
        hours = math.ceil(self.total_seconds() / seconds_per_hour)
        result = float(hours) / 24.0
        return result


class MonthWorktimeDict(dict):
    """
    Maps work time to individual months.
    Enables time addition
    and computes the lumptime and the reward time.
    """
    def __init__(self):
        self._lumplimit = 20.0 / 24.0

    def add_time(self, date, duration):
        "Adds the worktime given to the month of the date given"
        yearmonth = get_year_and_month_tuple(date)
        if yearmonth in self: # add the time given to the month entry
            self[yearmonth] = self[yearmonth] + duration
        else: # add the month entry and initialise it with the time given
            self[yearmonth] = duration

    def get_month_time(self, date):
        "Retuns the month worktime for the date given"
        yearmonth = get_year_and_month_tuple(date)
        if yearmonth in self: # get the time of the month of the date given
            return self[yearmonth]
        return 0

    def get_lumptime(self, date, tasktime):
        """
        Computes the lumptime for the date and tasktime given
        (may be 0, a part, or the whole tasktime)
        """
        monthtime = self.get_month_time(date)
        monthtimetaskincluded = monthtime + tasktime
        if monthtime <= self.lumplimit and \
        monthtimetaskincluded <= self.lumplimit:
            return tasktime
        if monthtime <= self.lumplimit and \
        monthtimetaskincluded >= self.lumplimit:
            return self.lumplimit - monthtime
        return 0

    def get_rewardtime(self, date, tasktime):
        """
        Computes the rewardtime for the date and tasktime given
        (may be 0, a part, or the whole tasktime)
        """
        monthtime = self.get_month_time(date)
        monthtimetaskincluded = monthtime + tasktime
        if monthtime <= self.lumplimit and \
        monthtimetaskincluded <= self.lumplimit:
            return 0
        if monthtime <= self.lumplimit and \
        monthtimetaskincluded >= self.lumplimit:
            return monthtimetaskincluded - self.lumplimit
        return tasktime

    @property
    def lumplimit(self):
        "The month lump limit"
        return self._lumplimit

    @lumplimit.setter
    def lumplimit(self, value):
        self._lumplimit = value

def create_instance(name, with_context=False):
    "Creates an instance of a service by the name given"
    if with_context:
        instance = SM.createInstanceWithContext(name, CTX)
    else:
        instance = SM.createInstance(name)
    return instance

def msgbox(
    parent, message, title='LibreOffice',
    buttons=MSG_BUTTONS.BUTTONS_OK,
    msg_type='infobox'
):
    """ Create message box
        type_msg: infobox, warningbox, errorbox, querybox, messbox
        https://api.libreoffice.org/docs/idl/ref/interfacecom_1_1sun_1_1star_1_1awt_1_1XMessageBoxFactory.html
    """
    toolkit = create_instance('com.sun.star.awt.Toolkit', False)
    mb = toolkit.createMessageBox(parent, msg_type, buttons, title, str(message))
    return mb.execute()

class InputBoxConstants: # pylint: disable=too-few-public-methods
    "Declares dimension constants for the inputbox function"
    WIDTH = 600
    HORI_MARGIN = VERT_MARGIN = 8
    BUTTON_WIDTH = 100
    BUTTON_HEIGHT = 26
    HORI_SEP = VERT_SEP = 8
    LABEL_HEIGHT = BUTTON_HEIGHT * 2 + 5
    EDIT_HEIGHT = 24
    HEIGHT = VERT_MARGIN * 2 + LABEL_HEIGHT + VERT_SEP + EDIT_HEIGHT
    LABEL_WIDTH = WIDTH - BUTTON_WIDTH - HORI_SEP - HORI_MARGIN * 2


def inputbox(ctx, message, title="", default="", x=None, y=None): # pylint: disable=too-many-arguments,too-many-locals
    """ Shows dialog with input box.
        @param message message to show on the dialog
        @param title window title
        @param default default value
        @param x dialog position in twips, pass y also
        @param y dialog position in twips, pass x also
        @return string if OK button pushed, otherwise zero length string
    """
    ibc = InputBoxConstants()
    def create(name):
        return ctx.getServiceManager().createInstanceWithContext(name, ctx)
    dialog = create("com.sun.star.awt.UnoControlDialog")
    dialog_model = create("com.sun.star.awt.UnoControlDialogModel")
    dialog.setModel(dialog_model)
    dialog.setVisible(False)
    dialog.setTitle(title)
    dialog.setPosSize(0, 0, ibc.WIDTH, ibc.HEIGHT, SIZE)
    def add(name, ctltype, x_, y_, width_, height_, props): # pylint: disable=too-many-arguments
        "Adds a control to the input box"
        model = dialog_model.createInstance(
            "com.sun.star.awt.UnoControl" + ctltype + "Model"
        )
        dialog_model.insertByName(name, model)
        control = dialog.getControl(name)
        control.setPosSize(x_, y_, width_, height_, POSSIZE)
        for key, value in props.items():
            setattr(model, key, value)
    add(
        "label", "FixedText", 
        ibc.HORI_MARGIN, ibc.VERT_MARGIN, ibc.LABEL_WIDTH, ibc.LABEL_HEIGHT,
        {"Label": str(message), "NoLabel": True}
    )
    add(
        "btn_ok", "Button", 
        ibc.HORI_MARGIN + ibc.LABEL_WIDTH + ibc.HORI_SEP, ibc.VERT_MARGIN,
        ibc.BUTTON_WIDTH, ibc.BUTTON_HEIGHT,
        {"PushButtonType": OK, "DefaultButton": True}
    )
    add(
        "btn_cancel", "Button", 
        ibc.HORI_MARGIN + ibc.LABEL_WIDTH + ibc.HORI_SEP,
        ibc.VERT_MARGIN + ibc.BUTTON_HEIGHT + 5,
        ibc.BUTTON_WIDTH, ibc.BUTTON_HEIGHT,
        {"PushButtonType": CANCEL}
    )
    add(
        "edit", "Edit", 
        ibc.HORI_MARGIN, ibc.LABEL_HEIGHT + ibc.VERT_MARGIN + ibc.VERT_SEP,
        ibc.WIDTH - ibc.HORI_MARGIN * 2, ibc.EDIT_HEIGHT,
        {"Text": str(default)}
    )
    frame = create("com.sun.star.frame.Desktop").getCurrentFrame()
    window = frame.getContainerWindow() if frame else None
    dialog.createPeer(create("com.sun.star.awt.Toolkit"), window)
    if not x is None and not y is None:
        ps = dialog.convertSizeToPixel(
            uno.createUnoStruct("com.sun.star.awt.Size", x, y), TWIP
        )
        _x, _y = ps.Width, ps.Height
    elif window:
        ps = window.getPosSize()
        _x = ps.Width / 2 - ibc.WIDTH / 2
        _y = ps.Height / 2 - ibc.HEIGHT / 2
    dialog.setPosSize(_x, _y, 0, 0, POS)
    edit = dialog.getControl("edit")
    edit.setSelection(
        uno.createUnoStruct("com.sun.star.awt.Selection", 0, len(str(default)))
    )
    edit.setFocus()
    ret = edit.getModel().Text if dialog.execute() else ""
    dialog.dispose()
    return ret

class ReportParamBoxConstants: # pylint: disable=too-few-public-methods
    "Declares dimension constants for the report parameter box function"
    WIDTH = 400
    SPACING = 8
    BUTTON_WIDTH = 100
    BUTTON_HEIGHT = 26
    EDIT_HEIGHT = 24
    LABEL_HEIGHT = EDIT_HEIGHT
    COMBO_HEIGHT = EDIT_HEIGHT
    HEIGHT = SPACING + (LABEL_HEIGHT + SPACING + COMBO_HEIGHT + SPACING) * 2 + \
        LABEL_HEIGHT + SPACING + EDIT_HEIGHT + SPACING
    LABEL_WIDTH = WIDTH - BUTTON_WIDTH - SPACING - SPACING * 2


def report_params_box(ctx): # pylint: disable=too-many-locals
    "An input dialog that gets the parameters for the diary import"
    rpbc = ReportParamBoxConstants()
    def create(name):
        return ctx.getServiceManager().createInstanceWithContext(name, ctx)
    dialog = create("com.sun.star.awt.UnoControlDialog")
    dialog_model = create("com.sun.star.awt.UnoControlDialogModel")
    dialog.setModel(dialog_model)
    dialog.setVisible(False)
    dialog.setTitle("Deník pilné včelky")
    dialog.setPosSize(0, 0, rpbc.WIDTH, rpbc.HEIGHT, SIZE)
    def add(name, ctltype, x_, y_, width_, height_, props): # pylint: disable=too-many-arguments
        model = dialog_model.createInstance(
            "com.sun.star.awt.UnoControl" + ctltype + "Model"
        )
        dialog_model.insertByName(name, model)
        control = dialog.getControl(name)
        control.setPosSize(x_, y_, width_, height_, POSSIZE)
        for key, value in props.items():
            setattr(model, key, value)
    add(
        "lbl_agenda", "FixedText",
        rpbc.SPACING, rpbc.SPACING, rpbc.LABEL_WIDTH, rpbc.LABEL_HEIGHT,
        {"Label": "Agenda", "NoLabel": True}
    )
    add(
        "cmb_agenda", "ListBox", 
        rpbc.SPACING, rpbc.SPACING + rpbc.LABEL_HEIGHT,
        rpbc.LABEL_WIDTH, rpbc.EDIT_HEIGHT,
        {
            "StringItemList": AGENDA_NAMES, 
            #"Text": "Korandův sbor",
            "Dropdown": True,
            "SelectedItems": (0,)
        }
    )
    add(
        "lbl_year", "FixedText",
        rpbc.SPACING, rpbc.SPACING * 2 + rpbc.LABEL_HEIGHT + rpbc.COMBO_HEIGHT,
        rpbc.LABEL_WIDTH, rpbc.LABEL_HEIGHT,
        {"Label": "Rok:", "NoLabel": True}
    )
    now = datetime.datetime.now()
    this_year = now.year
    this_month = now.month
    if this_month in range(2, 13):
        previous_month = this_month - 1
        year = this_year
    else:
        previous_month = 12
        year = this_year - 1
    s_year = str(year)
    add(
        "ed_year", "Edit", 
        rpbc.SPACING,
        rpbc.SPACING * 2 + rpbc.LABEL_HEIGHT * 2 + rpbc.COMBO_HEIGHT,
        rpbc.LABEL_WIDTH,
        rpbc.EDIT_HEIGHT,
        {"Text": s_year}
    )
    add(
        "lbl_month", "FixedText",
        rpbc.SPACING,
        rpbc.SPACING * 3 + rpbc.LABEL_HEIGHT * 2 + \
            rpbc.COMBO_HEIGHT + rpbc.EDIT_HEIGHT,
        rpbc.LABEL_WIDTH,
        rpbc.LABEL_HEIGHT,
        {"Label": "Měsíc:", "NoLabel": True}
    )
    lst_months = get_local_month_names()
    add(
        "cmb_month", "ListBox", 
        rpbc.SPACING,
        rpbc.SPACING * 3 + rpbc.LABEL_HEIGHT * 3 + \
            rpbc.COMBO_HEIGHT + rpbc.EDIT_HEIGHT,
        rpbc.LABEL_WIDTH,
        rpbc.EDIT_HEIGHT,
        {
            "StringItemList": lst_months, 
            #"Text": calendar.month_name[datetime.datetime.now().month - 2],
            "Dropdown": True,
            "SelectedItems": (previous_month - 1,)
        }
    )
    add(
        "btn_ok", "Button",
        rpbc.SPACING + rpbc.LABEL_WIDTH + rpbc.SPACING,
        rpbc.SPACING,
        rpbc.BUTTON_WIDTH,
        rpbc.BUTTON_HEIGHT,
        {"PushButtonType": OK, "DefaultButton": True}
    )
    add(
        "btn_cancel", "Button", 
        rpbc.SPACING + rpbc.LABEL_WIDTH + rpbc.SPACING,
        rpbc.SPACING + rpbc.BUTTON_HEIGHT + rpbc.SPACING,
        rpbc.BUTTON_WIDTH,
        rpbc.BUTTON_HEIGHT,
        {"PushButtonType": CANCEL}
    )
    frame = create("com.sun.star.frame.Desktop").getCurrentFrame()
    window = frame.getContainerWindow() if frame else None
    dialog.createPeer(create("com.sun.star.awt.Toolkit"), window)
    if window:
        ps = window.PosSize
        _x = ps.Width / 2 - rpbc.WIDTH / 2
        _y = ps.Height / 2 - rpbc.HEIGHT / 2
    dialog.setPosSize(_x, _y, 0, 0, POS)
    cmb_agenda = dialog.getControl("cmb_agenda")
    ed_year = dialog.getControl("ed_year")
    ed_year.setSelection(
        uno.createUnoStruct("com.sun.star.awt.Selection", 0, len(s_year))
    )
    cmb_month = dialog.getControl("cmb_month")
    #ed_year.setFocus()
    #ret = ed_year.getModel().Text if dialog.execute() else ""
    if dialog.execute():
        ret = {
            REPORT_PARAM_NAME_AGENDA:
                cmb_agenda.Model.SelectedItems[0],
            REPORT_PARAM_NAME_YEAR:
                ed_year.Model.Text,
            REPORT_PARAM_NAME_MONTH:
                cmb_month.Model.SelectedItems[0] + 1
        }
    else:
        ret = None
    dialog.dispose()
    return ret

def TestMessageBox(event): # pylint: disable=invalid-name,unused-argument
    "This is a test"
    s = "This is a test"
    t = "Test"
    res = msgbox(
        s, t, "querybox",
        MSG_BUTTONS.YES_NO_CANCEL + MSG_BUTTONS.DEF_NO
    )

    s = res
    msgbox(s, t, "infobox")

def ShowMessage(parent, msg_text=""): # pylint: disable=invalid-name
    "ShowMessage as in Delphi"
    msgbox(parent, msg_text)

#def ShowDir(obj):
#    ShowMessage(dirtext(obj, ", "))

def get_number_format(doc, format_string):
    """Get the number format to format cells."""
    o_formats = doc.Model.getNumberFormats()
    o_locale = uno.createUnoStruct("com.sun.star.lang.Locale")
    o_locale.Language = "cs"
    o_locale.Country = 203
    n_format_key = o_formats.queryKey(format_string, o_locale, True)
    if n_format_key == -1:
        n_format_key = o_formats.addNew(format_string, o_locale)
    return n_format_key

def get_date_format(doc):
    """Get the default date format to format the date cells as dates."""
    n_date_key = get_number_format(doc, "D. MMMM YYYY")
    return n_date_key

def get_time_format(doc):
    """Get the default time format to format the time cells as times."""
    n_time_key = get_number_format(doc, "[HH].MM")
    return n_time_key

def get_time_format_hms(doc):
    """Get the time format to format the time cells as times 
    with hours, minutes, and seconds."""
    n_time_key = get_number_format(doc, "[HH].MM.SS")
    return n_time_key

def get_time_format_decimal(doc):
    """Get the time format to format the time cells as times 
    in hours with decimal fractions."""
    n_time_key = get_number_format(doc, "Standard") # the default number format
                                                  # for the time being
    return n_time_key

def get_boolean_format(doc):
    """
    Get the default Boolean format to format the Boolean cells 
    as Booleans.
    """
    n_boolean_key = get_number_format(doc, "BOOLEAN")
    return n_boolean_key

def get_currency_format(doc):
    """
    Get the default currency format to format the amount cells
    as currency.
    """
    n_currency_key = get_number_format(
        doc,
        '# ##0,00" "[$Kč-405];[RED]-# ##0,00" "[$Kč-405]'
    )
    return n_currency_key

def get_relative_cell_name(component, cell):
    "Returns a relative address of the cell given"
    o_conv = component.createInstance(
        "com.sun.star.table.CellAddressConversion"
    )
    o_conv.Address = cell.CellAddress
    return o_conv.UserInterfaceRepresentation

def get_sheet_name(year, month):
    "Builds up a sheet name for the given year and month"
    # Get the local month names list
    month_names = get_local_month_names()
    # Compute the index of the given month in this list
    month_index = month - 1
    # Get the local month name
    month_name = month_names[month_index]
    # Capitalize the month name (only the first letter)
    capitalized_month_name = month_name.capitalize()
    # Build up the sheet name ("<Month name> <year>")
    result = f"{capitalized_month_name} {year}"
    return result

def get_empty_task():
    "Creates a new, empty task"
    result = BBDTask()
    result.category = MDASH
    result.duration = '00:00:00'
    result.id = -1
    result.isprivate = False
    result.name = MDASH
    result.notes = MDASH
    result.taskdate = MDASH
    return result

def read_tasks(recordset):
    "Reads the taksks from a recordset fetched from the database"
    def read_task():
        def find_field(fieldname):
            result = recordset.findColumn(fieldname)
            return result

        result = BBDTask()

        taskdatepos = find_field(BBD_FIELDNAME_TASKDATE)
        result.taskdate = recordset.getString(taskdatepos)

        idpos = find_field(BBD_FIELDNAME_ID)
        result.id = recordset.getLong(idpos)

        durationpos = find_field(BBD_FIELDNAME_DURATION)
        result.duration = recordset.getString(durationpos)

        categorypos = find_field(BBD_FIELDNAME_CATEGORY)
        result.category = recordset.getString(categorypos)

        namepos = find_field(BBD_FIELDNAME_NAME)
        result.name = recordset.getString(namepos)

        notespos = find_field(BBD_FIELDNAME_NOTES)
        result.notes = recordset.getString(notespos)

        privatepos = find_field(BBD_FIELDNAME_ISPRIVATE)
        result.isprivate = recordset.getString(privatepos)

        return result

    tasks = []
    if recordset is not None:
        while recordset.next():
            task = read_task()
            tasks.append(task)
    if len(tasks) == 0:
        empty_task = get_empty_task()
        tasks.append(empty_task)
    return tasks


class BBDTask(dict): # pylint: disable=too-many-instance-attributes
    """This is a model class for the Busy Bee Diary task"""
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.lumpduration = 0
        self.rewardduration = 0

    @property
    def taskdate(self):
        "The date of the task"
        return self[BBD_FIELDNAME_TASKDATE]

    @taskdate.setter
    def taskdate(self, value):
        self[BBD_FIELDNAME_TASKDATE] = value

    @property
    def id(self):
        "The ID of the task within its day"
        return self[BBD_FIELDNAME_ID]

    @id.setter
    def id(self, value):
        self[BBD_FIELDNAME_ID] = value

    @property
    def duration(self):
        "The duration of the task"
        return self[BBD_FIELDNAME_DURATION]

    @duration.setter
    def duration(self, value):
        self[BBD_FIELDNAME_DURATION] = value

    @property
    def category(self):
        "The category of the task"
        return self[BBD_FIELDNAME_CATEGORY]

    @category.setter
    def category(self, value):
        self[BBD_FIELDNAME_CATEGORY] = value

    @property
    def name(self):
        "The name of the task"
        return self[BBD_FIELDNAME_NAME]

    @name.setter
    def name(self, value):
        self[BBD_FIELDNAME_NAME] = value

    @property
    def notes(self):
        "The notes associated with the task"
        return self[BBD_FIELDNAME_NOTES]

    @notes.setter
    def notes(self, value):
        self[BBD_FIELDNAME_NOTES] = value

    @property
    def isprivate(self):
        "The private flag of the task"
        return self[BBD_FIELDNAME_ISPRIVATE]

    @isprivate.setter
    def isprivate(self, value):
        self[BBD_FIELDNAME_ISPRIVATE] = value

    @property
    def lumpduration(self):
        "The lump duration of the task"
        return self[BBD_COLNAME_LUMPDURATION]

    @lumpduration.setter
    def lumpduration(self, value):
        self[BBD_COLNAME_LUMPDURATION] = value

    @property
    def rewardduration(self):
        "The reward duration of the task"
        return self[BBD_COLNAME_REWARDDURATION]

    @rewardduration.setter
    def rewardduration(self, value):
        self[BBD_COLNAME_REWARDDURATION] = value

class BusyBeeDiaryReport:
    "The basic class for the Busy Bee Diary reports"
    def __init__(self, ctx):
        self.ctx = ctx
        self.cols = [
            BBD_COLNAME_TASKDATE,
            #BBD_COLNAME_ID,
            BBD_COLNAME_LUMPDURATION,
            BBD_COLNAME_REWARDDURATION,
            BBD_COLNAME_OFFICEDURATION,
            BBD_COLNAME_CATEGORY,
            BBD_COLNAME_NAME,
            BBD_COLNAME_NOTES,
            #BBD_COLNAME_ISPRIVATE
        ]
        self.category_filter = 'KS%'
        self.comp = None
        self.doc = None
        self.sheet = None
        self.worktimedict = MonthWorktimeDict()

    def create_uno_service(self, service_name):
        "Creates a service instance"
        ctx = self.ctx
        servman = ctx.ServiceManager
        return servman.createInstance(service_name)

    def create_uno_service_with_bbd_context(self, service_name):
        "Creates a service instance with this report’s context"
        ctx = self.ctx
        servman = ctx.ServiceManager
        return servman.createInstanceWithContext(service_name, ctx)

    def create_statement(self):
        "Creates a database statement"
        database_context = self.create_uno_service(
            "com.sun.star.sdb.DatabaseContext"
        )

        datasource = database_context.getByName("bbd")

        if not datasource.IsPasswordRequired:
            connection = datasource.getConnection("", "")
        else:
            #interaction_handler = self.createUnoService(
            #    "com.sun.star.sdb.InteractionHandler"
            #)
            #connection = data_source.ConnectWithCompletion(interaction_handler)
            # For testing purposes:
            connection = datasource.getConnection(
                "bbd", "5wvMCDjbgNuT4FqzF3TMN8oxMg9NrJvDfJ6b"
            )

        statement = connection.createStatement()

        return statement

    def get_column_index(self, colname):
        "Returns the column index of the column name given"
        index = self.cols.index(colname)
        return index

    def trigger(self, year, month):
        "The import core action"
        desktop = self.create_uno_service_with_bbd_context(
            "com.sun.star.frame.Desktop"
        )

        comp = self.comp = desktop.CurrentComponent
        doc = self.doc = comp.CurrentController
        sheets = comp.Sheets
        sheetname = get_sheet_name(year, month)
        if (sheetname in sheets) and (sheets.Count > 1):
            sheets.removeByName(sheetname)
        if sheetname not in sheets:
            sheets.insertNewByName(sheetname, sheets.Count)
        sheet = self.sheet = sheets[sheetname]
        doc.ActiveSheet = sheet

        statement = self.create_statement()

        resultset = statement.executeQuery(
            f"select * from v_tasks "
                f"where isprivate = false "
                f"and duration > time '00:00' "
                f"and category like '{self.category_filter}' "
                f"and extract(year from taskdate) = {year} "
                f"and extract(month from taskdate) = {month}" 
        )

        self.write_header()

        tasks = read_tasks(resultset)

        self.worktimedict.lumplimit = 20.0 / 24.0
        for t, task in enumerate(tasks):
            self.write_row(task, t)

        self.write_footer(len(tasks) + 1)
        self.adjust_columns()
        self.adjust_rows()
        window = doc.ComponentWindow
        ShowMessage(window, 'Hotovo!')

    def write_header(self):
        "Writes the sheet header"
        sheet = self.sheet
        for i, colname in enumerate(self.cols):
            cell = sheet.getCellByPosition(i, 0)
            cell.String = BBD_HEADINGS[colname]
            apply_default_cell_format(cell)
            cell.CharWeight = FontWeight.BOLD

    def get_relative_sum_cell_name(self, col_name, row_index):
        """
        Returns the relative address of the sum cell
        given by column name and row index
        """
        colindex = self.cols.index(col_name)
        sum_cell = self.sheet.getCellByPosition(colindex, row_index)
        sum_cell_name = get_relative_cell_name(self.comp, sum_cell)
        return sum_cell_name

    def write_row(self, task, taskindex):
        """
        Writes a single row for the task given
        """
        # Create a day delta object
        daydelta = DayDelta.parse_hours(task.duration)
        # Round the hours up
        whole_hours_time = daydelta.total_whole_hours_in_days
        # Calculate the task’s lump time
        lumptime = self.worktimedict.get_lumptime(
            task.taskdate, whole_hours_time
        )
        # Set it on the task
        task.lumpduration = lumptime
        # Calculate the task’s reward time
        rewardtime = self.worktimedict.get_rewardtime(
            task.taskdate, whole_hours_time
        )
        # Set it on the task
        task.rewardduration = rewardtime
        # Add the whole hours time to the current task’s month
        self.worktimedict.add_time(task.taskdate, whole_hours_time)
        # Write the cells
        for c, col_name in enumerate(self.cols):
            cell = self.sheet.getCellByPosition(c, taskindex + 1)
            self.write_cell(cell, task, col_name)
        row = self.sheet.Rows[taskindex + 1]
        row.OptimalHeight = True

    def write_cell(self, cell, task, col_name):
        """
        Fills a specific cell with the corresponding property of the task given
        """
        doc = self.doc
        cell.CharWeight = FontWeight.NORMAL
        apply_default_cell_format(cell)
        fieldname = BBD_COLUMNFIELDS[col_name]
        if col_name == "taskdate":
            taskdate = task.taskdate
            # Set the date as a formula
            cell.Formula = taskdate
            # Set the cellʼs date format
            cell.NumberFormat = get_date_format(doc)
            # Set the right alignment
            cell.HoriJustify = CellHoriJustify["RIGHT"]
        elif col_name == "id":
            cell.Formula = task.id
        elif col_name in ("name", "category", "notes"):
            cell.Formula = task[fieldname]
        elif fieldname == BBD_FIELDNAME_DURATION:
            if col_name == BBD_COLNAME_LUMPDURATION:
                # Set the cellʼs value to the lump time
                cell.Value = task.lumpduration
            elif col_name == BBD_COLNAME_REWARDDURATION:
                # Set the cellʼs value to the reward time
                cell.Value = task.rewardduration
            else: # maybe office time
                cell.Value = 0.0 # no means how to detect it yet
            # Set the cellʼs time format
            cell.NumberFormat = get_time_format(doc)
        elif col_name == "isprivate":
            cell.Formula = task.isprivate
            cell.NumberFormat = get_boolean_format(doc)

    def write_footer(self, row_index):
        "Writes the sheet footer"
        comp = self.comp
        sheet = self.sheet
        doc = self.doc
        for i, colname in enumerate(self.cols):
            cell = sheet.getCellByPosition(i, row_index)
            fieldname = BBD_COLUMNFIELDS[colname]
            if colname == BBD_COLNAME_TASKDATE: # sums label
                cell.String = "Celkem:"
                cell.HoriJustify = CellHoriJustify["RIGHT"]
            elif fieldname == BBD_FIELDNAME_DURATION: # time sums
                # Calculate the sum of the current column
                # Get the start and the end of the range
                start_cell = sheet.getCellByPosition(i, 1)
                start_cell_name = get_relative_cell_name(comp, start_cell)
                end_cell = sheet.getCellByPosition(i, row_index - 1)
                end_cell_name = get_relative_cell_name(comp, end_cell)
                # Set the sum formula
                cell.Formula = f"=SUM({start_cell_name}:{end_cell_name})"
                # Set the cellʼs time format
                cell.NumberFormat = get_time_format(doc)
            elif colname == BBD_COLNAME_CATEGORY: # salary estimate label
                self.write_salary_estimate_label(cell)
            elif colname == BBD_COLNAME_NAME: # salary estimate
                self.write_salary_estimate(cell)
            cell.CharWeight = FontWeight.BOLD
            apply_default_cell_format(cell)

    def write_salary_estimate_label(self, cell):
        "Writes the salary estimate label to a neighbouring cell given"
        cell.String = "Odhad mzdy:"
        cell.HoriJustify = CellHoriJustify["RIGHT"]

    def write_salary_estimate(self, cell):
        "Writes the salary estimate to the cell given"
        # Example: =4000+C24*24*350+D24*24*100
        doc = self.doc
        row_index = cell.CellAddress.Row
        # Get the cell addresses
        reward_cell_name = self.get_relative_sum_cell_name(
            BBD_COLNAME_REWARDDURATION, row_index
        )
        office_cell_name = self.get_relative_sum_cell_name(
            BBD_COLNAME_OFFICEDURATION, row_index
        )
        # Set the salary formula
        cell.Formula = "=4000+%(reward)s*24*350+%(office)s*24*100" % {
            "reward": reward_cell_name,
            "office": office_cell_name
        }
        cell.NumberFormat = get_currency_format(doc)
        cell.HoriJustify = CellHoriJustify["LEFT"]

    def adjust_columns(self):
        "Adjusts the columns to an optimal width"
        sheet = self.sheet
        for i, colname in enumerate(self.cols):
            fieldname = BBD_COLUMNFIELDS[colname]
            column = sheet.Columns[i]
            if colname in (
                "taskdate", "id", "category", "isprivate"
            ) or fieldname == BBD_FIELDNAME_DURATION:
                column.IsTextWrapped = False
                column.OptimalWidth = True
            elif colname in ("name", "notes"):
                column.IsTextWrapped = True
                column.Width = 10000

    def get_last_data_row(self):
        "Returns the number of the last row of the sheet’s used area"
        cursor = self.sheet.createCursor()
        cursor.gotoEndOfUsedArea(False)
        result = cursor.RangeAddress.EndRow
        return result

    def adjust_rows(self):
        """
        Adjusts row heights
        (but still doen’t work perfectly)
        """
        sheet = self.sheet
        last_row = self.get_last_data_row()
        for i in range(0, last_row + 1):
            row = sheet.Rows[i]
            row.OptimalHeight = True


class KorandaCongregationReport(BusyBeeDiaryReport):
    """
    The Koranda Congregation Report class.
    It’s basically equal to the basic report class.
    """

class EvangnetReport(BusyBeeDiaryReport):
    """
    The report class for the Evangnet Association
    """
    def __init__(self, ctx):
        BusyBeeDiaryReport.__init__(self, ctx)
        self.cols = [
            BBD_COLNAME_TASKDATE,
            #BBD_COLNAME_ID,
            BBD_COLNAME_REAL_DURATION_HMS,
            BBD_COLNAME_REAL_DURATION_H,
            BBD_COLNAME_BILLED_DURATION_HMS,
            BBD_COLNAME_BILLED_DURATION_H,
            BBD_COLNAME_CATEGORY,
            BBD_COLNAME_NAME,
            BBD_COLNAME_NOTES,
            #BBD_COLNAME_ISPRIVATE
        ]
        self.category_filter = 'Evangnet%'
        self.salary_per_hour = 260

    def get_hms_column_index(self, decimal_hours_colname):
        """
        Returns the index of the column with the hour-min-sec time
        by the corresponding decimal hours column name given
        """
        hmscolname = BBD_HMS_COLNAMES[decimal_hours_colname]
        hmscolindex = self.get_column_index(hmscolname)
        return hmscolindex

    def get_hms_cell(self, decimal_hours_cell):
        """
        Returns the hour-min-sec cell for a corresponding decimal hours cell
        """
        decimal_hours_colindex = decimal_hours_cell.CellAddress.Column
        decimal_hours_colname = self.cols[decimal_hours_colindex]
        hmscolindex = self.get_hms_column_index(decimal_hours_colname)
        rowindex = decimal_hours_cell.CellAddress.Row
        hmscell = self.sheet.getCellByPosition(hmscolindex, rowindex)
        return hmscell

    def get_hms_cell_address(self, decimal_hours_cell):
        """
        Returns the hour-min-sec cell address 
        for a corresponding decimal hours cell
        """
        hmscell = self.get_hms_cell(decimal_hours_cell)
        hmscelladdress = get_relative_cell_name(self.comp, hmscell)
        return hmscelladdress

    def get_decimal_hours_cell_formula(self, cell):
        """
        Returns the formula for the decimal hours cell
        """
        # Get the source cell address (it’s the h.m.s cell)
        hmscelladdress = self.get_hms_cell_address(cell)
        # Compose the formula
        formula = f'={hmscelladdress}*24'
        return formula

    def write_header(self):
        sheet = self.sheet
        for i, colname in enumerate(self.cols):
            cell = sheet.getCellByPosition(i, 0)
            cell.String = BBD_HEADINGS[colname]
            apply_default_cell_format(cell)
            cell.CharWeight = FontWeight.BOLD
            cell.HoriJustify = CellHoriJustify['CENTER']
            cell.IsTextWrapped = True


    def write_cell(self, cell, task, col_name):
        doc = self.doc
        cell.CharWeight = FontWeight.NORMAL
        apply_default_cell_format(cell)
        fieldname = BBD_COLUMNFIELDS[col_name]
        if col_name == "taskdate":
            taskdate = task.taskdate
            # Set the date as a formula
            cell.Formula = taskdate
            # Set the cellʼs date format
            cell.NumberFormat = get_date_format(doc)
            # Set the right alignment
            cell.HoriJustify = CellHoriJustify["RIGHT"]
        elif col_name == "id":
            cell.Formula = task.id
        elif col_name in ("name", "category", "notes"):
            cell.Formula = task[fieldname]
        elif fieldname == BBD_FIELDNAME_DURATION:
            # Get the task duration as a day delta
            daydelta = DayDelta.parse_hours(task.duration)
            # The task duration in days
            days = daydelta.total_days
            if col_name in (
                BBD_COLNAME_REAL_DURATION_HMS,
                BBD_COLNAME_BILLED_DURATION_HMS
            ):
                # Set the cellʼs value to the task duration in days
                cell.Value = days
                # Set the cellʼs time format to h.m.s
                cell.NumberFormat = get_time_format_hms(doc)
            else:
                # Set the cellʼs formula to hours with decimal fraction
                formula = self.get_decimal_hours_cell_formula(cell)
                cell.Formula = formula
                # Set the cell’s time format to decimal hours
                cell.NumberFormat = get_time_format_decimal(doc)
        elif col_name == "isprivate":
            cell.Formula = task.isprivate
            cell.NumberFormat = get_boolean_format(doc)

    def write_footer(self, row_index):
        comp = self.comp
        sheet = self.sheet
        doc = self.doc
        for i, colname in enumerate(self.cols):
            cell = sheet.getCellByPosition(i, row_index)
            fieldname = BBD_COLUMNFIELDS[colname]
            if colname == BBD_COLNAME_TASKDATE: # sums label
                cell.String = "Celkem:"
                cell.HoriJustify = CellHoriJustify["RIGHT"]
            elif fieldname == BBD_FIELDNAME_DURATION: # time sums
                # Calculate the sum of the current column
                # Get the start and the end of the range
                start_cell = sheet.getCellByPosition(i, 1)
                start_cell_name = get_relative_cell_name(comp, start_cell)
                end_cell = sheet.getCellByPosition(i, row_index - 1)
                end_cell_name = get_relative_cell_name(comp, end_cell)
                # Set the sum formula
                cell.Formula = f"=SUM({start_cell_name}:{end_cell_name})"
                # Set the cellʼs time format
                if colname in (
                    BBD_COLNAME_REAL_DURATION_HMS,
                    BBD_COLNAME_BILLED_DURATION_HMS
                ):
                    cell.NumberFormat = get_time_format_hms(doc)
                else:
                    cell.NumberFormat = get_time_format_decimal(doc)
            elif colname == BBD_COLNAME_CATEGORY: # salary estimate label
                self.write_salary_estimate_label(cell)
            elif colname == BBD_COLNAME_NAME: # salary estimate
                self.write_salary_estimate(cell)
            cell.CharWeight = FontWeight.BOLD
            apply_default_cell_format(cell)

    def write_salary_estimate_label(self, cell):
        cell.String = "Cena celkem:"
        cell.HoriJustify = CellHoriJustify["RIGHT"]

    def write_salary_estimate(self, cell):
        # Example: =3500+C24*24*350+D24*24*100
        doc = self.doc
        row_index = cell.CellAddress.Row
        # Get the cell addresses
        billed_hours_cell_name = self.get_relative_sum_cell_name(
            BBD_COLNAME_BILLED_DURATION_H, row_index
        )
        # Set the salary formula
        cell.Formula = f"={billed_hours_cell_name}*{self.salary_per_hour}"
        cell.NumberFormat = get_currency_format(doc)
        cell.HoriJustify = CellHoriJustify["LEFT"]

    def adjust_columns(self):
        sheet = self.sheet
        for i, colname in enumerate(self.cols):
            fieldname = BBD_COLUMNFIELDS[colname]
            column = sheet.Columns[i]
            if colname in (
                "taskdate", "id", "category", "isprivate"
            ) or fieldname == BBD_FIELDNAME_DURATION:
                column.IsTextWrapped = False
                header_cell = sheet.getCellByPosition(i, 0)
                header_cell.IsTextWrapped = True
                column.OptimalWidth = True
            elif colname in ("name", "notes"):
                column.IsTextWrapped = True
                column.Width = 10000


class WestBohemianSeniorateReport(BusyBeeDiaryReport):
    """
    The report class for the West-Bohemian Seniorate
    of the Evangelical Church of the Bohemian Brethern
    """
    def __init__(self, ctx):
        super().__init__(ctx)
        self.cols = [
            BBD_COLNAME_TASKDATE,
            #BBD_COLNAME_ID,
            BBD_COLNAME_EDITATION_DURATION,
            BBD_COLNAME_ADMINISTRATION_DURATION,
            BBD_COLNAME_CATEGORY,
            BBD_COLNAME_NAME,
            BBD_COLNAME_NOTES,
            #BBD_COLNAME_ISPRIVATE
        ]
        self.category_filter = 'ZČSČCE%'

    def write_cell(self, cell, task, col_name):
        doc = self.doc
        cell.CharWeight = FontWeight.NORMAL
        apply_default_cell_format(cell)
        fieldname = BBD_COLUMNFIELDS[col_name]
        if col_name == "taskdate":
            taskdate = task.taskdate
            # Set the date as a formula
            cell.Formula = taskdate
            # Set the cellʼs date format
            cell.NumberFormat = get_date_format(doc)
            # Set the right alignment
            cell.HoriJustify = CellHoriJustify["RIGHT"]
        elif col_name == "id":
            cell.Formula = task.id
        elif col_name in ("name", "category", "notes"):
            cell.Formula = task[fieldname]
        elif fieldname == BBD_FIELDNAME_DURATION:
            if (
                (
                    col_name == BBD_COLNAME_EDITATION_DURATION
                    and
                    task.category in ['ZČSČCE-editace', 'ZČSČCE-kalendář']
                )
                or
                (
                    col_name == BBD_COLNAME_ADMINISTRATION_DURATION
                    and
                    task.category == 'ZČSČCE-správa'
                )
            ):
                # Get the task duration as a day delta
                daydelta = DayDelta.parse_hours(task.duration)
                # Round the hours up
                whole_hours_time = daydelta.total_whole_hours_in_days
                # Set the cellʼs value to the reward time
                cell.Value = whole_hours_time
            else:
                cell.Value = 0
            # Set the cellʼs time format
            cell.NumberFormat = get_time_format(doc)
        elif col_name == "isprivate":
            cell.Formula = task.isprivate
            cell.NumberFormat = get_boolean_format(doc)

    def write_salary_estimate_label(self, cell):
        cell.String = "Cena celkem:"
        cell.HoriJustify = CellHoriJustify["RIGHT"]

    def write_salary_estimate(self, cell):
        # Example: =3500+C24*24*350+D24*24*100
        doc = self.doc
        row_index = cell.CellAddress.Row
        # Get the cell addresses
        editation_cell_name = self.get_relative_sum_cell_name(
            BBD_COLNAME_EDITATION_DURATION, row_index
        )
        administration_cell_name = self.get_relative_sum_cell_name(
            BBD_COLNAME_ADMINISTRATION_DURATION, row_index
        )
        # Set the salary formula
        cell.Formula = "=%(editation)s*24*100+%(administration)s*24*200" % {
            "editation": editation_cell_name,
            "administration": administration_cell_name
        }
        cell.NumberFormat = get_currency_format(doc)
        cell.HoriJustify = CellHoriJustify["LEFT"]

class IcebergReport(BusyBeeDiaryReport):
    """
    The report class for the Iceberg Association
    """
    def __init__(self, ctx):
        super().__init__(ctx)
        self.cols = [
            BBD_COLNAME_TASKDATE,
            #BBD_COLNAME_ID,
            BBD_COLNAME_DURATION,
            BBD_COLNAME_CATEGORY,
            BBD_COLNAME_NAME,
            BBD_COLNAME_NOTES,
            #BBD_COLNAME_ISPRIVATE
        ]
        self.category_filter = 'Ledovec%'

    def write_cell(self, cell, task, col_name):
        doc = self.doc
        cell.CharWeight = FontWeight.NORMAL
        apply_default_cell_format(cell)
        fieldname = BBD_COLUMNFIELDS[col_name]
        if col_name == "taskdate":
            taskdate = task.taskdate
            # Set the date as a formula
            cell.Formula = taskdate
            # Set the cellʼs date format
            cell.NumberFormat = get_date_format(doc)
            # Set the right alignment
            cell.HoriJustify = CellHoriJustify["RIGHT"]
        elif col_name == "id":
            cell.Formula = task.id
        elif col_name in ("name", "category", "notes"):
            cell.Formula = task[fieldname]
        elif fieldname == BBD_FIELDNAME_DURATION:
            # Get the task duration as a day delta
            daydelta = DayDelta.parse_hours(task.duration)
            # Round the hours up
            whole_hours_time = daydelta.total_whole_hours_in_days
            # Set the cellʼs value to the reward time
            cell.Value = whole_hours_time
            # Set the cellʼs time format
            cell.NumberFormat = get_time_format(doc)
        elif col_name == BBD_COLNAME_ISPRIVATE:
            cell.Formula = task.isprivate
            cell.NumberFormat = get_boolean_format(doc)

    def write_salary_estimate_label(self, cell):
        cell.Formula = ""


    def write_salary_estimate(self, cell):
        # Example: =3500+C24*24*350+D24*24*100
        doc = self.doc
        #row_index = cell.CellAddress.Row
        # Set the salary formula
        cell.Formula = ""
        cell.NumberFormat = get_currency_format(doc)
        cell.HoriJustify = CellHoriJustify["LEFT"]


class SnowReport(IcebergReport):
    """
    A report class for the Snow Initiative Association
    """
    def __init__(self, ctx):
        super().__init__(ctx)
        self.category_filter = 'Sníh%'

    def write_salary_estimate_label(self, cell):
        cell.String = "Cena celkem:"
        cell.HoriJustify = CellHoriJustify["RIGHT"]

    def write_salary_estimate(self, cell):
        # Example: =C24*24*260
        doc = self.doc
        row_index = cell.CellAddress.Row
        # Get the cell addresses
        duration_cell_name = self.get_relative_sum_cell_name(
            BBD_COLNAME_DURATION, row_index
        )
        # Set the salary formula
        cell.Formula = f"={duration_cell_name}*24*260"
        cell.NumberFormat = get_currency_format(doc)
        cell.HoriJustify = CellHoriJustify["LEFT"]

class WerxReport(EvangnetReport):
    """
    A report class for the WERX SYSTEM, s. r. o. company
    """
    def __init__(self, ctx):
        super().__init__(ctx)
        self.category_filter = 'Werx%'
        self.salary_per_hour = 350



AGENDA_REPORT_CLASSES = [
    KorandaCongregationReport,
    WestBohemianSeniorateReport,
    EvangnetReport,
    IcebergReport,
    SnowReport,
    WerxReport
]

class BusyBeeDiary(unohelper.Base, XJobExecutor):
    """
    The runner class that asks for the agenda and other parameters
    then it runs the corresponding report with these parameters as arguments
    """
    def __init__(self, ctx):
        super().__init__()
        self.ctx = ctx

    def trigger(self, args): # pylint: disable=unused-argument
        """
        Asks for the report & params then runs the report chosen
        """
        report_params = report_params_box(self.ctx)

        if report_params:
            agenda = report_params[REPORT_PARAM_NAME_AGENDA]
            year   = report_params[REPORT_PARAM_NAME_YEAR]
            month  = report_params[REPORT_PARAM_NAME_MONTH]

            report_class = AGENDA_REPORT_CLASSES[agenda]
            report = report_class(self.ctx)
            report.trigger(year, month)


def RunBusyBeeDiary(): # pylint: disable=invalid-name
    "The runner function"
    ctx = uno.getComponentContext()
    diary = BusyBeeDiary(ctx)
    diary.trigger(())

def BusyBeeDiaryEventHandler(event): # pylint: disable=invalid-name,unused-argument
    "Handles the LibreOffice event"
    RunBusyBeeDiary()

def apply_default_cell_format(cell):
    """Apply default cell format"""
    # Font name
    cell.CharFontName = "Times New Roman"
    # Font size
    cell.CharHeight = 12
    # Horizontal alignment
    cell.HoriJustify = CellHoriJustify["LEFT"]
    # Vertical alignment
    cell.VertJustify = CellVertJustify["TOP"]
    ###########
    # Borders #
    ###########
    # Grab the left border line as a pattern
    line = cell.LeftBorder
    # Make it gray
    line.Color = 0x808080
    # Inner and outer line widths
    line.InnerLineWidth = 2
    line.OuterLineWidth = 2
    # Distance between the lines
    line.LineDistance = 0x23
    # Double-line style
    line.LineStyle = 3
    # Overall line width
    line.LineWidth = 0x27
    # Set all the border lines to the pattern line
    cell.LeftBorder = line
    cell.TopBorder = line
    cell.RightBorder = line
    cell.BottomBorder = line


g_ImplementationHelper = unohelper.ImplementationHelper()
g_ImplementationHelper.addImplementation(
    BusyBeeDiary,
    "cz.borova18.BusyBeeDiary",
    ("com.sun.star.task.Job",)
)

if __name__ == "__main__":
    # Get the test document path
    # Ectract the directory containing this script
    SCRIPT_DIRECTORY = os.path.dirname(__file__)
    TEST_DOCUMENT_PATH = os.path.join(SCRIPT_DIRECTORY, "BusyBeeTest.ods")

    # Start OpenOffice.org, listen for connections and open the testing document
    os.system(
        f"loffice '--accept=socket,host=localhost,port=2002;urp;' --calc "
        f"{TEST_DOCUMENT_PATH} &"
    )
    # Get local context info
    LOCAL_CONTEXT = uno.getComponentContext()
    RESOLVER = LOCAL_CONTEXT.ServiceManager.createInstanceWithContext(
        "com.sun.star.bridge.UnoUrlResolver", LOCAL_CONTEXT )
    CONTEXT = None
    # Wait until LibreOffice starts and connection is established
    while CONTEXT is None:
        try:
            CONTEXT = RESOLVER.resolve(
                "uno:socket,host=localhost,port=2002;urp;"
                "StarOffice.ComponentContext" 
            )
        except NoConnectException:
            pass
    # Wait until the document gets open
    DESKTOP = CONTEXT.ServiceManager.createInstanceWithContext(
        "com.sun.star.frame.Desktop", CONTEXT
    )
    DOC = DESKTOP.getCurrentComponent()
    while DOC is None:
        DOC = DESKTOP.getCurrentComponent()
    # Trigger our job
    #RunBusyBeeDiary()
    DIARY = BusyBeeDiary(CONTEXT)
    DIARY.trigger(())
