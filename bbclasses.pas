unit BBClasses;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fgl, BBUtils;

type

  { TTaskCategory }

  TTaskCategory = class
    constructor Create;
  private
    FID: Integer;
    FName: String;
    procedure SetID(AValue: Integer);
    procedure SetName(AValue: String);
  published
    property ID: Integer read FID write SetID;
    property Name: String read FName write SetName;
  end;

  TTaskCategoryListBase = specialize TFPGObjectList<TTaskCategory>;

  { TTaskCategoryList }

  TTaskCategoryList = class(TTaskCategoryListBase)
  public
    function FindById(ID: Integer): Integer;
    function CategoryByID(ID: Integer): TTaskCategory;
    function FindByName(Name: String): Integer;
    function CategoryByName(Name: String): TTaskCategory;
    function AddCategory(var Category: TTaskCategory): Integer;
    function GetNewCategoryID: Integer;
  end;

  { TTask }

  TTask = class
  private
    FCategoryList: TTaskCategoryList;
    FTaskDate: TDate;
    FID: Integer;
    FIsPrivate: Boolean;
    FDuration: TDateTime;
    FCategoryID: Integer;
    FName: String;
    FNotes: String;
    function GetCategory: TTaskCategory;
    procedure SetCategoryID(AValue: Integer);
    procedure SetCategoryList(AValue: TTaskCategoryList);
    procedure SetID(AValue: Integer);
    procedure SetName(AValue: String);
    procedure SetNotes(AValue: String);
    procedure SetTaskDate(AValue: TDate);
    procedure SetIsPrivate(AValue: Boolean);
    procedure SetDuration(AValue: TDateTime);
  public
    constructor Create;
  published
    property TaskDate: TDate read FTaskDate write SetTaskDate;
    property ID: Integer read FID write SetID;
    property IsPrivate: Boolean read FIsPrivate write SetIsPrivate;
    property Duration: TDateTime read FDuration write SetDuration;
    property Name: String read FName write SetName;
    property CategoryID: Integer read FCategoryID write SetCategoryID;
    property Notes: String read FNotes write SetNotes;
    property CategoryList: TTaskCategoryList read FCategoryList
      write SetCategoryList;
    property Category: TTaskCategory read GetCategory;
  end;

implementation

{ TTaskCategoryList }

function TTaskCategoryList.FindById(ID: Integer): Integer;
var
  I: Integer;
  Category: TTaskCategory;
begin
  Result := -1;
  for I := 0 to Pred(Count) do
  begin
    Category := Self[I];
    if Category.ID = ID then
    begin
      Result := I;
      Break;
    end;
  end;
end;

function TTaskCategoryList.CategoryByID(ID: Integer): TTaskCategory;
var
  Index: Integer;
begin
  Index:=FindById(ID);
  Result := Self[Index];
end;

function TTaskCategoryList.FindByName(Name: String): Integer;
var
  I: Integer;
  Category: TTaskCategory;
begin
  Result := -1;
  for I := 0 to Pred(Count) do
  begin
    Category := Self[I];
    if Category.Name = Name then
    begin
      Result := I;
      Break;
    end;
  end;
end;

function TTaskCategoryList.CategoryByName(Name: String): TTaskCategory;
var
  Index: Integer;
begin
  Index:=FindByName(Name);
  Result := Self[Index];
end;

function TTaskCategoryList.AddCategory(var Category: TTaskCategory): Integer;
var
  FoundIndex: Integer;
  FoundCategory: TTaskCategory;
begin
  // First, investigate if a category with the same name already exists
  FoundIndex:=FindByName(Category.Name);
  if FoundIndex > -1 {already exists} then
  begin
    FoundCategory := Self[FoundIndex];
    // Change the object
    if Category <> FoundCategory then
    begin
      Category.Free;
      Category := FoundCategory;
    end;
    Result := FoundIndex;
    DebugMsgFmt(
      'Category already exists: ID: %d, Name: “%s”.',
      [Category.ID, Category.Name]
    );
  end
  else // not found -> insert or update
  begin
    if Category.ID = -1 {it's new} then
    begin
      Category.ID:=GetNewCategoryID; // give it a fresh ID
      Result := Add(Category); // insert it
      DebugMsgFmt(
        'Adding a new category: ID: %d, Name: “%s”.',
        [Category.ID, Category.Name]
      );
    end
    else
    begin
      FoundIndex:=FindByID(Category.ID);
      FoundCategory:=Self[FoundIndex];
      // Replace the category found
      if Category <> FoundCategory then
      begin
        FoundCategory.Free;
        Self[FoundIndex] := Category; // update it
      end;
      Result := FoundIndex;
      DebugMsgFmt(
        'Updating an existing category: ID: %d, Name: “%s”.',
        [Category.ID, Category.Name]
      );
    end;
  end;
end;

function TTaskCategoryList.GetNewCategoryID: Integer;
var
  I: Integer;
  Category: TTaskCategory;
  MaxID: Integer;
begin
  MaxID := -1;
  for I := 0 to Pred(Count) do
  begin
    Category := Self[I];
    if Category.ID > MaxID then
    begin
      MaxID:=Category.ID;
    end;
  end;
  Result := MaxID + 1;
end;

{ TTaskCategory }

procedure TTaskCategory.SetName(AValue: String);
begin
  if FName=AValue then Exit;
  FName:=AValue;
end;

constructor TTaskCategory.Create;
begin
  FID:=-1;
  FName:='';
end;

procedure TTaskCategory.SetID(AValue: Integer);
begin
  if FID=AValue then Exit;
  FID:=AValue;
end;

{ TTask }

procedure TTask.SetName(AValue: String);
begin
  if FName=AValue then Exit;
  FName:=AValue;
end;

procedure TTask.SetNotes(AValue: String);
begin
  if FNotes=AValue then Exit;
  FNotes:=AValue;
end;

procedure TTask.SetCategoryID(AValue: Integer);
begin
  if FCategoryID=AValue then Exit;
  FCategoryID:=AValue;
end;

function TTask.GetCategory: TTaskCategory;
begin
  if FCategoryList <> nil then
  begin
    if FCategoryID > -1 then
      Result := FCategoryList.CategoryByID(FCategoryID)
    else
      Result := nil;
  end
  else
    Result := nil;
end;

procedure TTask.SetCategoryList(AValue: TTaskCategoryList);
begin
  if FCategoryList=AValue then Exit;
  FCategoryList:=AValue;
end;

procedure TTask.SetID(AValue: Integer);
begin
  if FID=AValue then Exit;
  FID:=AValue;
end;

procedure TTask.SetTaskDate(AValue: TDate);
begin
  if FTaskDate=AValue then Exit;
  FTaskDate:=AValue;
end;

procedure TTask.SetIsPrivate(AValue: Boolean);
begin
  if FIsPrivate=AValue then Exit;
  FIsPrivate:=AValue;
end;

procedure TTask.SetDuration(AValue: TDateTime);
begin
  if FDuration = AValue then Exit;
  FDuration:=AValue;
end;

constructor TTask.Create;
begin
  inherited Create;
  FID:=-1;
end;

end.

