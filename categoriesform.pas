unit CategoriesForm;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, StdCtrls,
  Buttons, DBGrids,
  DataModule, sqldb, db,
  LCLType, DBCtrls;

type

  TCategoryAction = (
    caNone,
    caNew,
    caEdit,
    caCopy,
    caDelete
  );

  { TFrmCategories }

  TFrmCategories = class(TForm)
    DBNavigator1: TDBNavigator;
    DS: TDataSource;
    Grid: TDBGrid;
    Query: TSQLQuery;
    Transaction: TSQLTransaction;
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure GridDblClick(Sender: TObject);
    procedure GridKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure QueryNewRecord(DataSet: TDataSet);
  private
    FInitialID: Integer;
    procedure DeleteCurrentRecord;
    procedure DoDeleteRecord(ID: Integer);
    function GetFreeID: Integer;
    function GoToID(ID: Integer): Boolean;
  public
    function Execute(InitialID: Integer = -1): Integer;
  end;

var
  FrmCategories: TFrmCategories;

implementation

{$R *.lfm}

{ TFrmCategories }

procedure TFrmCategories.FormShow(Sender: TObject);
begin
  //Grid.ViewData();
  Query.Open;
  GoToID(FInitialID);
end;

procedure TFrmCategories.GridDblClick(Sender: TObject);
begin
  ModalResult:=mrOK;
end;

procedure TFrmCategories.GridKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  DataSetState: TDataSetState;
begin
   case Key of
   VK_F2:
      begin
        Grid.EditorMode:=True;
      end;
   VK_RETURN:
      begin
        if not Grid.EditorMode then
        begin
          Key:=0;
          DataSetState:=Query.State;
          case DataSetState of
          dsInsert, dsEdit:
            begin
              Query.Post;
              Query.ApplyUpdates;
            end;
          else
          end;
          ModalResult:=mrOK;
        end;
      end;
   else
   end;
end;

procedure TFrmCategories.QueryNewRecord(DataSet: TDataSet);
var
  NewID: Integer;
begin
  NewID:=GetFreeID;
  DataSet.FieldByName('id').AsInteger:=NewID;
  Grid.SelectedField := Query.FieldByName('name');
end;

procedure TFrmCategories.FormClose(Sender: TObject;
  var CloseAction: TCloseAction);
begin
  //Query.Close;
end;

procedure TFrmCategories.DeleteCurrentRecord;
var
  ID: Integer;
  Question: String;
  Response: TModalResult;
  CategoryName: String;
begin
  // Get the category identifier
  ID := Query.FieldByName('id').AsInteger;
  // and the name
  CategoryName := Query.FieldByName('name').AsString;
  // Prepare the confirmative question
  Question := Format(
    'Do you really want to delete the selected category (“%s”)?' +
    LineEnding +
    'It could lead to loss of category data if any tasks in the ' +
    'selected category exist!',
    [CategoryName]
  );
  // Get the users’s choice
  Response := MessageDlg(
    'Category Deletion', Question, mtWarning, mbYesNo, 0, mbNo
  );
  // If he or she chose yes, really delete the category.
  if Response = mrYes then
  begin
    DoDeleteRecord(ID);
  end;
end;

procedure TFrmCategories.DoDeleteRecord(ID: Integer);
begin
  DM.SQLQuery.Params.Clear;
  DM.SQLQuery.SQL.Text := 'delete from categories where id=:id';
  DM.SQLQuery.ParamByName('id').AsInteger := ID;
  DM.SQLQuery.ExecSQL;
  DM.SQLTransaction.Commit;
end;

function TFrmCategories.GetFreeID: Integer;
{ TODO : Maybe base this on a stored procedure? }
var
  ID: Integer;
  Found: Integer;
  IsFree: Boolean;
begin
  ID:=-1;
  // Prepare the query
  DM.SQLQuery.SQL.Text:='select count(*) as found from categories ' +
    'where id = :id';
  repeat
    Inc(ID);
    // Fill in the params
    DM.SQLQuery.ParamByName('id').AsInteger:=ID;
    // Get the found records count for the proposed id
    // from the first record (should be always present)
    // 0 -> not found -> The ID is free, let's take it.
    // 1 ->     found -> The ID is taken, let's continue our search.
    // more -> What the heck? Impossible!
    DM.SQLQuery.Open;
    DM.SQLQuery.First;
    Found:=DM.SQLQuery.FieldByName('found').AsInteger;
    IsFree:=Found = 0;
    DM.SQLQuery.Close;
  until IsFree;
  Result := ID;
end;

function TFrmCategories.GoToID(ID: Integer): Boolean;
begin
  Result := Query.Locate('id', ID, []);
end;

function TFrmCategories.Execute(InitialID: Integer): Integer;
var
  locModalResult: TModalResult;
begin
  FInitialID:=InitialID;
  locModalResult := Self.ShowModal;
  if locModalResult = mrOK then
    Result := Query.FieldByName('id').AsInteger
  else
    Result := InitialID;
end;

end.

