unit DataModule;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, pqconnection, sqldb, db, BBClasses, BBUtils,
  RachotaClasses;

type

  TPasswordKind = (
    pkBBD,
    pkRoot
  );

  TPasswordStrings = array[TPasswordKind] of String;

  TPasswordRecord = record
    Password: String;
    Match: Boolean;
  end;

  TPasswordRecords = array[TPasswordKind] of TPasswordRecord;

  { TDM }

  TDM = class(TDataModule)
    DataSource: TDataSource;
    PQConnection: TPQConnection;
    PQRootConnection: TPQConnection;
    SQLQuery: TSQLQuery;
    SQLScript: TSQLScript;
    SQLTransaction: TSQLTransaction;
    SQLRootTransaction: TSQLTransaction;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    FPasswordRecords: TPasswordRecords;
    function GetBBDMatch: Boolean;
    function GetBBDPassword: String;
    function GetRootMatch: Boolean;
    function GetRootPassword: String;
    procedure SetBBDMatch(AValue: Boolean);
    procedure SetBBDPassword(AValue: String);
    procedure SetRootMatch(AValue: Boolean);
    procedure SetRootPassword(AValue: String);
    property BBDPassword: String read GetBBDPassword
      write SetBBDPassword;
    property RootPassword: String read GetRootPassword
      write SetRootPassword;
    property BBDMatch: Boolean read GetBBDMatch
      write SetBBDMatch;
    property RootMatch: Boolean read GetRootMatch
      write SetRootMatch;
  public
    CategoryList: TTaskCategoryList;
    { Connecting and database creation }
    function Connect: Boolean;
    procedure CreateDB;
    function CreateUser: Boolean;
    function PromptForPassword(Kind: TPasswordKind;
      Caption: String = ''; Prompt: String = ''): Boolean;
    function PromptForPasswordIfEmpty(Kind: TPasswordKind;
      Caption: String = ''; Prompt: String = ''): Boolean;
    function PromptForPasswordIfNoMatch(Kind: TPasswordKind;
      Caption: String = ''; Prompt: String = ''): Boolean;
    { Task categories }
    function FindCategory(CategoryName: String): Integer;
    function CategoryIDExists(ID: Integer): Boolean;
    function GetNewCategoryID: Integer;
    procedure SaveCategoryViaUpdate(Category: TTaskCategory);
    procedure SaveCategoryViaInsert(Category: TTaskCategory);
    procedure SaveCategory(Category: TTaskCategory);
    function ReadCategory(ID: Integer): TTaskCategory;
    procedure ReadCategoryList;
    procedure SaveCategoryList;
    { Tasks }
    function TaskExists(TaskDate: TDate; ID: Integer): Boolean;
    function GetNewTaskID(TaskDate: TDate): Integer;
    procedure SaveTaskViaUpdate(Task: TTask);
    procedure SaveTaskViaInsert(Task: TTask);
    procedure SaveTask(Task: TTask);
    function ReadTask(TaskDate: TDate; ID: Integer): TTask;
  end;

var
  DM: TDM;

operator := (RachotaTask: TRachotaTask): TTask;

implementation

{$R *.lfm}

uses Dialogs, Clipbrd;

operator:=(RachotaTask: TRachotaTask): TTask;
begin
  Result := TTask.Create;
  Result.Name:=RachotaTask.Description;
  Result.Duration:=RachotaTask.Duration;
  Result.IsPrivate:=RachotaTask.IsPrivate;
  Result.Notes:=RachotaTask.Notes;
  if DM <> nil then
    Result.CategoryList:=DM.CategoryList;
  // CategoryID and TaskDate must be set from the outside
end;

{ TDM }

procedure TDM.DataModuleDestroy(Sender: TObject);
begin
  CategoryList.Free;
  PQConnection.Close();
end;

procedure TDM.DataModuleCreate(Sender: TObject);
begin
  CategoryList := TTaskCategoryList.Create();
end;

function TDM.GetBBDMatch: Boolean;
begin
  Result := FPasswordRecords[pkBBD].Match;
end;

function TDM.GetBBDPassword: String;
begin
  Result := FPasswordRecords[pkBBD].Password;
end;

function TDM.GetRootMatch: Boolean;
begin
  Result := FPasswordRecords[pkRoot].Match;
end;

function TDM.GetRootPassword: String;
begin
  Result := FPasswordRecords[pkRoot].Password;
end;

procedure TDM.SetBBDMatch(AValue: Boolean);
begin
  FPasswordRecords[pkBBD].Match:=AValue;
end;

procedure TDM.SetBBDPassword(AValue: String);
begin
  FPasswordRecords[pkBBD].Password:=AValue;
end;

procedure TDM.SetRootMatch(AValue: Boolean);
begin
  FPasswordRecords[pkRoot].Match:=AValue;
end;

procedure TDM.SetRootPassword(AValue: String);
begin
  FPasswordRecords[pkRoot].Password:=AValue;
end;

function TDM.Connect: Boolean;
var
  Msg: String;
  BBDInput: Boolean;
begin
  Result := False;
  PQConnection.HostName:='borova18.cz';
  PQConnection.DatabaseName:='bbd';
  PQConnection.UserName:='bbd';
  //PQConnection.Password:='5wvMCDjbgNuT4FqzF3TMN8oxMg9NrJvDfJ6b';
  {
  BBDInput := PromptForPasswordIfNoMatch(
    pkBBD,
    'Log In',
    'Please enter your password to the Busy Bee Diary:'
  );
  }
  // For testing purposes -->
  FPasswordRecords[pkBBD].Password:='5wvMCDjbgNuT4FqzF3TMN8oxMg9NrJvDfJ6b';
  FPasswordRecords[pkBBD].Match:=True;
  BBDInput:=True;
  // <--
  if BBDInput then
  begin
    PQConnection.Password:=BBDPassword;
    try
      PQConnection.Open;
      BBDMatch:=True;
      Result := True;
    except
      on E: EDatabaseError do
      begin
        Msg:=E.Message;
        if (Msg = 'PQConnection : Connection to database failed ' +
          '(PostgreSQL: FATAL:  password authentication failed for user ' +
          '"bbd"' + LineEnding +
          'FATAL:  password authentication failed for user ' +
          '"bbd"' + LineEnding +
          ')')
        or (Msg = 'PQConnection : Connection to database failed ' +
          '(PostgreSQL: fe_sendauth: no password supplied' + LineEnding +
          ')')
        then
        begin
          if Self.CreateUser then
            Result := Self.Connect;
        end
        else if Msg = 'PQConnection : Connection to database failed ' +
          '(PostgreSQL: FATAL:  database "bbd" does not exist' + LineEnding +
          ')'
        then
        begin
          ShowMessage('The database does not exist. Creating it.');
          //DM.PQConnection.DatabaseName:='postgres';
          //DM.PQConnection.UserName:='postgres';
          //DM.PQConnection.Password:='36K5juLmRTQyXTYBfJaMBVSNJWtD9yKH5Mch';
          Self.CreateDB;
          Result := Self.Connect;
        end
        else
        begin
          Clipboard.AsText:=E.Message;
          ShowMessageFmt('Error while opening the database. ' + LineEnding +
            'Class: %s' + LineEnding +
            'Message: %s',
            [
              E.ClassName,
              Msg
            ]
          );
        end;
      end;
    end;
  end;
end;

procedure TDM.CreateDB;
var
  BBDInput: Boolean;
  TransactionWasActive: Boolean;
begin
  TransactionWasActive:=SQLTransaction.Active;
  BBDInput:=PromptForPasswordIfNoMatch(pkBBD);
  if BBDInput then
  begin
    try
      PQConnection.Password:=BBDPassword;
      PQConnection.CreateDB;
      TransactionWasActive:=SQLTransaction.Active;
      SQLScript.ExecuteScript;
      SQLTransaction.Commit;
      BBDMatch:=True;

    finally
      if TransactionWasActive then SQLTransaction.StartTransaction;
    end;
  end;
end;

function TDM.CreateUser: Boolean;
var
  BBDInput: Boolean;
  RootInput: Boolean;
begin
  Result := False;
  PQRootConnection.HostName:='borova18.cz';
  PQRootConnection.DatabaseName:='postgres';
  PQRootConnection.UserName:='postgres';
  //PQRootConnection.Password:='36K5juLmRTQyXTYBfJaMBVSNJWtD9yKH5Mch';
  RootInput := PromptForPasswordIfNoMatch(
    pkRoot,
    'Log in as the database administrator',
    'It seems that you have problems loggging in.' + LineEnding +
    'Would you like to try to create a BBD regular user?' + LineEnding +
    'If so, please enter the database administrator (postgres) password:'
  );
  if RootInput then
  begin
    PQRootConnection.Password:=RootPassword;
    BBDInput:=PromptForPasswordIfNoMatch(
      pkBBD,
      'Create a password',
      'Please enter a new password for the Busy Bee Diary regular user' +
      '(and keep it safe!):'
    );
    if BBDInput then
    begin
      PQRootConnection.ExecuteDirect(
        Format(
          'create user bbd createdb encrypted password ''%s'';',
          [BBDPassword]
        )
      );
      SQLRootTransaction.Commit;
      BBDMatch:=True;
      RootMatch:=True;
      Result := True;
    end;
  end;
end;

function TDM.PromptForPassword(Kind: TPasswordKind; Caption: String = '';
  Prompt: String = ''): Boolean;
const
  Users: TPasswordStrings = (
    'bbd',
    'postgres'
  );
var
  Response: String;
begin
  if Prompt = '' then Prompt:='Log in as user ' + Users[Kind];
  Response := PasswordBox(Caption, Prompt);
  if Response > '' then
  begin
    FPasswordRecords[Kind].Password := Response;
    Result := True;
  end
  else
    Result := False;
end;

function TDM.PromptForPasswordIfEmpty(Kind: TPasswordKind; Caption: String = '';
  Prompt: String = ''): Boolean;
begin
  if FPasswordRecords[Kind].Password = '' then
  begin
    Result := PromptForPassword(Kind, Caption, Prompt);
  end
  else
    Result := True;
end;

function TDM.PromptForPasswordIfNoMatch(Kind: TPasswordKind; Caption: String;
  Prompt: String): Boolean;
begin
  if not FPasswordRecords[Kind].Match then
  begin
    Result := PromptForPassword(Kind, Caption, Prompt);
  end
  else
    Result := True;
end;

function TDM.FindCategory(CategoryName: String): Integer;
var
  RecordCount: Integer;
begin
  Result := -1;
  SQLQuery.SQL.Text:='select count(*) as recordcount from categories ' +
    'where name = :name';
  SQLQuery.Params.ParamByName('name').AsString := CategoryName;
  try
    SQLQuery.Open;
    SQLQuery.First;
    RecordCount := SQLQuery.FieldByName('recordcount').AsInteger;
    SQLQuery.Close;
    if RecordCount > 0 then
    begin
      SQLQuery.SQL.Text:='select * from categories where name = :categoryname';
      SQLQuery.Params.ParamByName('categoryname').AsString := CategoryName;
      try
        SQLQuery.Open;
        SQLQuery.First;
        Result := SQLQuery.FieldByName('id').AsInteger;
      finally
        if SQLQuery.Active then SQLQuery.Close;
      end;
    end;
  finally
    if SQLQuery.Active then SQLQuery.Close;
  end;
end;

function TDM.CategoryIDExists(ID: Integer): Boolean;
begin
  try
    SQLQuery.SQL.Text:='select count(*) as recordcount from categories ' +
      'where id = :id';
    SQLQuery.ParamByName('id').AsInteger:=ID;
    SQLQuery.Open;
    Result := SQLQuery.FieldByName('recordcount').AsInteger > 0;
  finally
    if SQLQuery.Active then SQLQuery.Close;
  end;
end;

function TDM.GetNewCategoryID: Integer;
begin
  try
    SQLQuery.SQL.Text:='select (max(id) + 1) as cid from categories';
    SQLQuery.Open;
    Result := SQLQuery.FieldByName('cid').AsInteger;
  finally
    if SQLQuery.Active then SQLQuery.Close;
  end;
end;

procedure TDM.SaveCategoryViaUpdate(Category: TTaskCategory);
begin
  SQLQuery.SQL.Text:='update categories set name = :name where id = :id';
  SQLQuery.ParamByName('id').AsInteger:=Category.ID;
  SQLQuery.ParamByName('name').AsString:=Category.Name;
  try
    SQLQuery.ExecSQL;
    SQLTransaction.Commit;
  finally
    if SQLQuery.Active then SQLQuery.Close;
  end;
end;

procedure TDM.SaveCategoryViaInsert(Category: TTaskCategory);
begin
  SQLQuery.SQL.Text:='insert into categories(id, name) ' +
    'values (:id, :name)';
  SQLQuery.ParamByName('id').AsInteger:=Category.ID;
  SQLQuery.ParamByName('name').AsString:=Category.Name;
  try
    SQLQuery.ExecSQL;
    SQLTransaction.Commit;
  finally
    if SQLQuery.Active then SQLQuery.Close;
  end;
end;

procedure TDM.SaveCategory(Category: TTaskCategory);
var
  IDExists: Boolean;
begin
  if Category.ID > - 1 {the ID is set} then
  begin
    IDExists:=CategoryIDExists(Category.ID);
    if IDExists {the ID is already present in the DB} then
    begin
      SaveCategoryViaUpdate(Category); // update it
      DebugMsgFmt(
        'Updating an existing category: ID: %d, Name: “%s”.',
        [Category.ID, Category.Name]
      );
    end
    else // we’ve got a new ID
    begin
      SaveCategoryViaInsert(Category); // insert it
      DebugMsgFmt(
        'Adding a new category: ID: %d, Name: “%s”.',
        [Category.ID, Category.Name]
      );
    end;
  end
  else // ID is not set
    DebugMsgFmt(
      'Attempt to save a category with an invalid ID: %d, Name: “%s”.',
      [Category.ID, Category.Name]
    );
end;

function TDM.ReadCategory(ID: Integer): TTaskCategory;
begin
  Result := nil;
  if CategoryIDExists(ID) then
  begin
    SQLQuery.SQL.Text:='select * from categories where id = :id';
    SQLQuery.ParamByName('id').AsInteger:=ID;
    try
      SQLQuery.Open;
      Result := TTaskCategory.Create;
      Result.ID:=ID;
      Result.Name := SQLQuery.FieldByName('name').AsString;
    finally
      if SQLQuery.Active then SQLQuery.Close;
    end;
  end;
end;

procedure TDM.ReadCategoryList;
var
  Category: TTaskCategory;
begin
  CategoryList.Clear;
  SQLQuery.SQL.Text:='select * from categories';
  try
    SQLQuery.Open;
    SQLQuery.Last;
    SQLQuery.First;
    while not SQLQuery.EOF do
    begin
      Category := TTaskCategory.Create;
      Category.ID:=SQLQuery.FieldByName('id').AsInteger;
      Category.Name:=SQLQuery.FieldByName('name').AsString;
      CategoryList.AddCategory(Category);
    end;
  finally
    if SQLQuery.Active then SQLQuery.Close;
  end;
end;

procedure TDM.SaveCategoryList;
var
  I: Integer;
  Category: TTaskCategory;
begin
  for I := 0 to Pred(CategoryList.Count) do
  begin
    Category := CategoryList[I];
    SaveCategory(Category);
  end;
end;

function TDM.TaskExists(TaskDate: TDate; ID: Integer): Boolean;
begin
  try
    SQLQuery.SQL.Text:='select count(*) as recordcount from tasks ' +
      'where taskdate = :taskdate and id = :id';
    SQLQuery.ParamByName('taskdate').AsDate:=TaskDate;
    SQLQuery.ParamByName('id').AsInteger:=ID;
    SQLQuery.Open;
    Result := SQLQuery.FieldByName('recordcount').AsInteger > 0;
  finally
    if SQLQuery.Active then SQLQuery.Close;
  end;
end;

function TDM.GetNewTaskID(TaskDate: TDate): Integer;
begin
  try
    SQLQuery.SQL.Text:='select (max(id) + 1) as tid from tasks ' +
      'where taskdate = :taskdate';
    SQLQuery.ParamByName('taskdate').AsDate:=TaskDate;
    SQLQuery.Open;
    Result := SQLQuery.FieldByName('tid').AsInteger;
  finally
    if SQLQuery.Active then SQLQuery.Close;
  end;
end;

procedure TDM.SaveTaskViaUpdate(Task: TTask);
begin
  SQLQuery.SQL.Text:='update tasks set name = :name, ' +
    'category = :category, duration = :duration, isprivate = :isprivate, ' +
    'notes = :notes ' +
    'where taskdate = :taskdate and id = :id';
  SQLQuery.ParamByName('id').AsInteger:=Task.ID;
  SQLQuery.ParamByName('taskdate').AsDate:=Task.TaskDate;
  SQLQuery.ParamByName('name').AsString:=Task.Name;
  SQLQuery.ParamByName('category').AsInteger:=Task.CategoryID;
  SQLQuery.ParamByName('duration').AsDateTime:=Task.Duration;
  SQLQuery.ParamByName('isprivate').AsBoolean:=Task.IsPrivate;
  SQLQuery.ParamByName('notes').AsString:=Task.Notes;
  try
    SQLQuery.ExecSQL;
    SQLTransaction.Commit;
  finally
    if SQLQuery.Active then SQLQuery.Close;
  end;
end;

procedure TDM.SaveTaskViaInsert(Task: TTask);
begin
  SQLQuery.SQL.Text:='insert into tasks' +
    '(id, taskdate, name, category, duration, isprivate, notes) ' +
    'values (:id, :taskdate, :name, :category, :duration, :isprivate, :notes)';
  SQLQuery.ParamByName('id').AsInteger:=Task.ID;
  SQLQuery.ParamByName('taskdate').AsDate:=Task.TaskDate;
  SQLQuery.ParamByName('name').AsString:=Task.Name;
  SQLQuery.ParamByName('category').AsInteger:=Task.CategoryID;
  SQLQuery.ParamByName('duration').AsDateTime:=Task.Duration;
  SQLQuery.ParamByName('isprivate').AsBoolean:=Task.IsPrivate;
  SQLQuery.ParamByName('notes').AsString:=Task.Notes;
  try
    SQLQuery.ExecSQL;
    SQLTransaction.Commit;
  finally
    if SQLQuery.Active then SQLQuery.Close;
  end;
end;

procedure TDM.SaveTask(Task: TTask);
begin
  if Task.ID = -1 {it's new} then
  begin
    Task.ID:=GetNewTaskID(Task.TaskDate); // give it a fresh ID
    SaveTaskViaInsert(Task); // insert it
    DebugMsgFmt(
      'Adding a new task: ID: %d, Name: “%s”.',
      [Task.ID, Task.Name]
    );
  end
  else // it already has an ID assigned
  begin
    if TaskExists(Task.TaskDate, Task.ID) then
    begin
      SaveTaskViaUpdate(Task); // update it
      DebugMsgFmt(
        'Updating an existing task: ID: %d, Name: “%s”.',
        [Task.ID, Task.Name]
      );
    end
    else
    begin
      SaveTaskViaInsert(Task); // insert it
      DebugMsgFmt(
        'Adding a new task: ID: %d, Name: “%s”.',
        [Task.ID, Task.Name]
      );
    end;
  end;
end;

function TDM.ReadTask(TaskDate: TDate; ID: Integer): TTask;
begin
  Result := nil;
  if TaskExists(TaskDate, ID) then
  begin
    SQLQuery.SQL.Text:='select * from tasks where id = :id';
    SQLQuery.ParamByName('id').AsInteger:=ID;
    try
      SQLQuery.Open;
      Result := TTask.Create;
      Result.ID:=ID;
      Result.TaskDate:=SQLQuery.FieldByName('taskdate').AsDateTime;
      Result.Name:=SQLQuery.FieldByName('name').AsString;
      Result.CategoryID:=SQLQuery.FieldByName('category').AsInteger;
      Result.Duration:=SQLQuery.FieldByName('duration').AsDateTime;
      Result.IsPrivate:=SQLQuery.FieldByName('isprivate').AsBoolean;
      Result.Notes:=SQLQuery.FieldByName('notes').AsString;
    finally
      if SQLQuery.Active then SQLQuery.Close;
    end;
  end;
end;



end.

