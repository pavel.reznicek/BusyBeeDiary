unit BBUtils;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, StdCtrls;

type

  TDebugMsgType = (
    dmtMessage,
    dmtWarning
  );

  TDebugMsgTypes = set of TDebugMsgType;

var
  DebugMsgTypes: TDebugMsgTypes = [dmtMessage, dmtWarning];
  DebugMemo: TMemo;

procedure DebugMsg(Message: String);
procedure DebugMsgFmt(Message: String; Values: array of const);
procedure Warning(Message: String);
procedure WarningFmt(Message: String; Values: array of const);

resourcestring
  txtWarning = 'Warning: ';

implementation

procedure DebugMsg(Message: String);
begin
  if dmtMessage in DebugMsgTypes then
  begin
    if DebugMemo <> nil then
    begin
      DebugMemo.Append(Message);
      DebugMemo.Update;;
      DebugMemo.VertScrollBar.Position:=DebugMemo.VertScrollBar.Range;
    end
    else
      WriteLn(Message);
  end;
end;

procedure DebugMsgFmt(Message: String; Values: array of const);
begin
  DebugMsg(Format(Message, Values));
end;

procedure Warning(Message: String);
begin
  if dmtWarning in DebugMsgTypes then
  begin
    if DebugMemo <> nil then
    begin
      DebugMemo.Append(Message);
      DebugMemo.Update;
    end
    else
      WriteLn(txtWarning, Message);
  end;
end;

procedure WarningFmt(Message: String; Values: array of const);
begin
  Warning(Format(Message, Values));
end;

end.

